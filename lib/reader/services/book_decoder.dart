import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:readeo/models/language.model.dart';
import 'package:readeo/models/toc.model.dart' as Toc;
import 'package:readeo/reader/models/chapter.dart';
import 'package:readeo/reader/models/sentence.dart';
import 'package:readeo/models/book.model.dart';

import 'package:html/parser.dart' show parse;

class BookDecoder {
  final sendChannel =
      BasicMessageChannel('com.miost.readeo/icu/data', StringCodec());
  final List<Chapter> chapters = List();
  final List<Toc.Chapter> toc = List();
  final BookModel bookModel;
  final Function onChapterRead;
  final LanguageModel language;

  BookDecoder(this.bookModel, this.language, {this.onChapterRead}) {
    _readChapter();
  }

  List<String> get chapterList => toc.map((c) => c.name).toList();

  loadChapter(int chapterPosition) async {
    _readCachedChapter(chapterPosition);
  }

  readBook() async {
    toc.clear();

    var tocFile = await File("${bookModel.folderPath}/toc.json").readAsString();

    toc.addAll((jsonDecode(tocFile) as List)
        .map((e) => e == null ? null : Toc.Chapter.fromJson(e))
        .toList());
  }

  _readChapter() {
    sendChannel.setMessageHandler((String message) => Future<String>(() {
          _decodeChapterMessage(message);
          return message;
        }));
  }

  _decodeChapterMessage(String message) {
    var decodedMessage = jsonDecode(message);
    var sentences = (decodedMessage["sentences"] as List)
        ?.map((e) =>
            e == null ? null : Sentence.fromJson(e as Map<String, dynamic>))
        ?.toList();
    var pos = decodedMessage["position"] as int;

    chapters.add(Chapter(pos, sentences));

    _saveCachedChapter(pos, message);

    onChapterRead(pos, sentences);
  }

  _saveCachedChapter(int pos, String cachedContent) async {
    String path = "/${bookModel.folderPath}/c$pos.cache";

    await File(path).writeAsString(cachedContent);
  }

  _readCachedChapter(int pos) async {
    String path = "/${bookModel.folderPath}/c$pos.cache";

    bool isFileExist = await File(path).exists();
    if (isFileExist) {
      _decodeChapterMessage(await File(path).readAsString());
    } else {
      _parseChapter(pos);
    }
  }

  _parseChapter(int chapterPosition) async {
    var txt = parse(
            await File("${bookModel.folderPath}/${toc[chapterPosition].href}")
                .readAsString()).body.text.trim();

    if (txt == null || txt.isEmpty) txt = "·";

    Map<String, dynamic> _json = Map();
    _json["position"] = chapterPosition;
    _json["type"] = "tokenize";
    _json["text"] = txt;
    _json["languageCode"] = language.code3;
    sendChannel.send(json.encode(_json));
  }

  dispose() {
    toc.clear();
    chapters.clear();
  }
}
