import 'package:translator/translator.dart';

class TranslationService {
  Future<String> getTranslation(String text, String langFrom, String langTo,
      String langToFallback) async {
    // TODO: change to a proper langTo
    langTo = 'pl';
    var translator = GoogleTranslator();
    var resp = await translator.translate(text, to: langTo, from: langFrom);

    return resp;
  }
}
