import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/models/language.model.dart';
import 'package:readeo/reader/services/book_decoder.dart';
import 'package:readeo/reader/models/page_style.dart';
import 'package:readeo/reader/models/page.dart' as ReaderModel;
import 'package:readeo/reader/paging/pages_calculator.dart';
import 'package:readeo/reader/providers/reader_token_store_provider.dart';
import 'package:readeo/reader/models/sentence.dart';
import 'package:readeo/reader/models/token.dart';
import 'package:readeo/reader/models/token_sentence_tuple.dart';
import 'package:readeo/models/book.model.dart';
import 'package:readeo/models/token.model.dart';

import 'package:path_provider/path_provider.dart';

class ReaderProvider extends ChangeNotifier {
  final sendChannel =
      BasicMessageChannel('com.miost.readeo/icu/data', StringCodec());

  BookDecoder bookDecoder;
  PagesCalculator pagesCalculator;
  ReaderTokenStoreProvider tokenStore;
  StreamSubscription tokensStream;

  LanguageModel _language;
  BookModel book;
  List<String> get chapterList => bookDecoder?.chapterList ?? [];
  MyDatabase _db;

  Size pageSize;
  PageStyle _currentPageStyle = PageStyle();
  PageStyle get currentPageStyle => _currentPageStyle;

  int _currentPageNumber = 0;
  int get currentPageNumber => _currentPageNumber;

  bool _initialized = false;

  final pages = List<ReaderModel.Page>();

  final _stream = StreamController<List<ReaderModel.Page>>.broadcast();
  Stream get pagesStream => _stream.stream;

  // FlutterTts flutterTts = FlutterTts();

  int get currentProgress {
    print(
        "${currentPage.location.start} ${pages.lastWhere((p) => p.chapter == currentPage.chapter).location.start}");
    try {
      return (currentPage.location.start /
              pages
                  .lastWhere((p) => p.chapter == currentPage.chapter)
                  .location
                  .start *
              100)
          .round();
    } catch (e) {
      return 0;
    }
  }

  ReaderModel.Page get currentPage => pages[_currentPageNumber];
  String get currentChapterName {
    if (chapterList.isEmpty) return book.title;

    return chapterList[currentPage.chapter];
  }

  int get getPageByLocation => _currentPageNumber = pages.indexWhere((p) =>
      p.chapter == book.currentChapter &&
      p.location.start <= book.currentLocation &&
      p.location.end > book.currentLocation);

  ReaderModel.Page getPage(int position) {
    if (position + 2 > pages.length) {
      bookDecoder.loadChapter(pages.last.chapter + 1);
    }

    return pages[position];
  }

  Future<void> initialize(
      MyDatabase db, BookModel bm, ReaderTokenStoreProvider ts) async {
    if (!_initialized) {
      book = bm;
      tokenStore = ts;
      _db = db;
      _language = await _db.languagesDao.getLanguage(book.langCode);

      bm.rootFolder = (await getApplicationDocumentsDirectory()).path;

      bookDecoder =
          BookDecoder(book, _language, onChapterRead: (pos, sentences) async {
        var _pages = await PagesCalculator(
                pageSize: pageSize, currentPageStyle: _currentPageStyle)
            .calculatePages(pos, sentences);

        if (pages.isEmpty || (_pages.first.chapter > pages.first.chapter)) {
          pages.addAll(_pages);
        } else {
          pages.insertAll(0, _pages);
          _currentPageNumber += _pages.length - 1;
        }

        _stream.sink.add(pages);
      });

      await bookDecoder.readBook();

      tokensStream = db.tokensDao.watchListOfTokens(filters: [
        (u) => u.languageCode.equals(book.langCode)
      ]).listen((data) {
        tokenStore.listOfTokens.clear();
        tokenStore.lang = book.langCode;
        tokenStore.listOfTokens.addAll(Map.fromIterable(data,
            key: (e) => e.name, value: (e) => TokenModel.fromToken(e)));
      });

      // flutterTts.setLanguage(book.langCode);

      bookDecoder.loadChapter(bm.currentChapter);
      _initialized = true;
    }

    return;
  }

  changeChapter(int chapterNum) {
    pages.clear();
    bookDecoder.chapters.clear();
    _currentPageNumber = 0;

    // _stream.sink.add(pages);
    bookDecoder.loadChapter(chapterNum);
  }

  setCurrentPageNumber(int p) async {
    _currentPageNumber = p;
    book.currentChapter = pages[p].chapter;
    book.currentLocation = pages[p].location.start;
    await _db.booksDao.updateBook(book);
  }

  saveUnknownTokens(int level) async {
    List<DbToken> dbtokens = List();

    currentPage.tokenStrings.forEach((f) {
      if (!tokenStore.listOfTokens.containsKey(f)) {
        TokenModel tm = TokenModel();
        tm.langcode = book.langCode;
        tm.name = f;
        tm.definition = '';
        tm.level = level;
        tm.updatedAt = DateTime.now();
        dbtokens.add(tm.toDbToken());
      }
    });

    if (dbtokens.isNotEmpty) {
      await _db.tokensDao.upsertAll(dbtokens);
      notifyListeners();
    }
  }

  TokenSentenceTuple getEditableToken(Token token) {
    String name = token.text.toLowerCase();

    TokenModel _t = TokenModel();
    _t.name = name;
    _t.definition = ' ';
    _t.langcode = book.langCode;
    _t.level = 0;

    TokenModel tm = tokenStore.listOfTokens.containsKey(name)
        ? tokenStore.listOfTokens[name]
        : _t;

    Sentence s = bookDecoder.chapters
        .firstWhere(((c) => c.position == currentPage.chapter), orElse: null)
        .sentenceForTokenLocation(token.start, token.end);

    return TokenSentenceTuple(tm, s, token.start, _language.code2);
  }

  saveToken(TokenSentenceTuple tokenSentenceTuple) async {
    tokenSentenceTuple.token.updatedAt = DateTime.now();
    print(tokenSentenceTuple.token.definition);

    await _db.tokensDao.upsertToken(tokenSentenceTuple.token.toDbToken());
    notifyListeners();
  }

  speak(String text) {
    // flutterTts.speak(text);
  }

  @override
  dispose() {
    super.dispose();
    _stream.close();
    bookDecoder.dispose();
    tokensStream.cancel();
    // flutterTts.stop();
    _language = null;
    _initialized = false;
  }
}
