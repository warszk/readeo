import 'package:flutter/widgets.dart';

class PageStyle {
  TextAlign textAlign = TextAlign.justify;
  double fontSize = 16.0;
  double lineHeight = 1.75;
  TextDirection textDirection = TextDirection.ltr;
}
