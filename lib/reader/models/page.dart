import 'package:readeo/reader/models/token.dart';

class PageLocation {
  int start;
  int end;

  PageLocation({this.start, this.end});

  @override
  String toString() {
    return "location: <$start, $end>";
  }
}

class Page {
  final int chapter;
  final PageLocation location;
  final List<Token> tokens;

  Page({this.location, this.tokens, this.chapter});

  Set<String> get tokenStrings => tokens
      .where((t) => t.type.toLowerCase() == "word")
      .map((t) => t.text.toLowerCase())
      .toSet();

  @override
  String toString() {
    return tokens.map((t) => t.text).join('');
  }
}
