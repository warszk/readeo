import 'package:flutter/widgets.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:readeo/reader/models/token.dart';

part 'sentence.g.dart';

@JsonSerializable()
class Sentence {
  int start;
  int end;
  List<Token> tokens;

  Sentence(this.start, this.end, this.tokens);

  factory Sentence.fromJson(Map<String, dynamic> json) =>
      _$SentenceFromJson(json);
  Map<String, dynamic> toJson() => _$SentenceToJson(this);

  @override
  String toString() {
    return tokens.map((t) => t.text).join('');
  }

  String toText() {
    return tokens.map((t) => t.text).join('');
  }

  TextSpan boldWord(int start, TextStyle baseStyle) {
    var toks = tokens.map((token) {
      if (token.start == start) {
        return TextSpan(
            text: token.text,
            style: baseStyle.copyWith(fontWeight: FontWeight.bold));
      } else {
        return TextSpan(text: token.text, style: baseStyle);
      }
    }).toList();

    return TextSpan(text: '', children: toks, style: baseStyle);
  }

  List<Token> getTokensForLocation(int tStart, int tEnd) {
    if (start >= tStart-500 && end <= tEnd+500) {
      return tokens.where((t) => t.start >= tStart && t.end <= tEnd).toList();
    }

    return List<Token>();
  }
}
