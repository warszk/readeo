import 'package:readeo/reader/models/sentence.dart';
import 'package:readeo/models/token.model.dart';

class TokenSentenceTuple {
  final int locationStart;
  final String langCode2;
  final TokenModel token;
  final Sentence sentence;

  TokenSentenceTuple(this.token, this.sentence, this.locationStart, this.langCode2);
}
