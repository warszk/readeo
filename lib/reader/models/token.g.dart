// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Token _$TokenFromJson(Map<String, dynamic> json) {
  return Token(
    json['start'] as int,
    json['end'] as int,
    json['type'] as String,
    json['text'] as String,
    json['level'] as int,
  );
}

Map<String, dynamic> _$TokenToJson(Token instance) => <String, dynamic>{
      'start': instance.start,
      'end': instance.end,
      'type': instance.type,
      'text': instance.text,
      'level': instance.level,
    };
