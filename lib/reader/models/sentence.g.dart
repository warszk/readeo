// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sentence.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sentence _$SentenceFromJson(Map<String, dynamic> json) {
  return Sentence(
    json['start'] as int,
    json['end'] as int,
    (json['tokens'] as List)
        ?.map(
            (e) => e == null ? null : Token.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$SentenceToJson(Sentence instance) => <String, dynamic>{
      'start': instance.start,
      'end': instance.end,
      'tokens': instance.tokens,
    };
