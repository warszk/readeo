import 'package:readeo/reader/models/sentence.dart';

class Chapter {
  final int position;
  final List<Sentence> sentences;

  Chapter(this.position, this.sentences);

  Sentence sentenceForTokenLocation(int start, int end) =>
      sentences.firstWhere((s) => s.start <= start && s.end >= end);
}
