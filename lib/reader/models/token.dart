import 'package:json_annotation/json_annotation.dart';

part 'token.g.dart';

enum TokenType { WORD, OTHER }

@JsonSerializable()
class Token {
  int start;
  int end;
  String type;
  String text;
  int level = 0;

  Token(this.start, this.end, this.type, this.text, this.level);

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);
  Map<String, dynamic> toJson() => _$TokenToJson(this);

  @override
  String toString() {
    return "$text ($start; $end)";
  }
}
