import 'package:flutter/material.dart';
import 'package:readeo/reader/models/page_style.dart';

class Paging {
  Size size;
  int maxLines;
  PageStyle pageStyle;
  TextPainter _textPainter;

  Paging({this.size, this.maxLines, this.pageStyle}) {
    if(maxLines == null) {
      maxLines = estimatedMaxLines;
    }

    _textPainter = TextPainter(textDirection: pageStyle.textDirection, textAlign: pageStyle.textAlign, maxLines: maxLines);
  }

  bool layout(String text) {
    _textPainter
      ..text = TextSpan(text: text, style: TextStyle(fontSize: pageStyle.fontSize, height: pageStyle.lineHeight))
      ..layout(maxWidth: size.width);

    return _textPainter.didExceedMaxLines || _textPainter.size.height > size.height;
  }

  int get maxLength {
    return _textPainter
      .getPositionForOffset(size.bottomRight(Offset.zero))
      .offset;
  }

  int get estimatedMaxLines {
    TextPainter tp = TextPainter(
        textDirection: pageStyle.textDirection,
        textAlign: pageStyle.textAlign
    );

    tp.text = TextSpan(
        text: '',
        children: [
          TextSpan(
              text: 'A',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: pageStyle.fontSize,
                  height: pageStyle.lineHeight
              )
          )
        ]
    );
    tp.layout(maxWidth: size.width);

    return ((size.height - 20) / tp.height).floor();
  }
}
