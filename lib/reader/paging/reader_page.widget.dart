import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/reader/models/page.dart' as ReaderModel;
import 'package:readeo/reader/models/page_style.dart';
import 'package:readeo/reader/providers/reader_token_store_provider.dart';
import 'package:readeo/reader/models/token.dart';
import 'package:readeo/shared/colors.dart';

class ReaderPage extends StatelessWidget {
  final PageStyle style;
  final Size size;
  final Function(Token token) onTokenTap;
  final ReaderModel.Page page;

  ReaderPage({this.style, this.size, this.onTokenTap, this.page});

  @override
  Widget build(BuildContext context) {
    final tokenStore = Provider.of<ReaderTokenStoreProvider>(context);

    var _painter = ReaderCustomPainter(
        textStyle: TextStyle(
            fontSize: style.fontSize,
            height: style.lineHeight,
            color: Colors.black),
        page: page,
        tokenStore: tokenStore);

    return GestureDetector(
      child: Center(
          child: CustomPaint(
        painter: _painter,
        size: Size(size.width, size.height),
      )),
      onTapDown: (details) {
        if (onTokenTap != null) {
          Token tok = _painter.getWordForPosition(details.localPosition);
          if (tok != null) onTokenTap(tok);
        }
      },
    );
  }
}

class ReaderCustomPainter extends CustomPainter {
  final TextStyle textStyle;
  ReaderModel.Page page;
  ReaderTokenStoreProvider tokenStore;

  ReaderCustomPainter({this.textStyle, this.page, this.tokenStore});
  TextPainter textPainter;

  @override
  void paint(Canvas canvas, Size size) {
    textPainter = TextPainter(
        text: cleanTextForGestureRecognition(),
        textDirection: TextDirection.ltr);
    textPainter.layout(
      maxWidth: size.width,
    );

    textPainter.paint(canvas, Offset(0, 0));

    var t = TextPainter(text: pageTextSpan(), textDirection: TextDirection.ltr);
    t.layout(
      maxWidth: size.width,
    );
    t.paint(canvas, Offset(0, 0));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  // we need this because on the bug in flutter's way to handle gesture recognition of nested TextSpans
  TextSpan cleanTextForGestureRecognition() {
    var style = textStyle.copyWith(color: ReaderColors.bg);
    // var style = textStyle;
    var txt = page.tokens.map((t) => t.text).join('').trim();
    return TextSpan(text: txt, style: style);
  }

  TextSpan pageTextSpan() {
    var toks = page.tokens;

    var tokenTrimmed = true;
    while (tokenTrimmed) {
      if (toks.first.text.trim().isEmpty) {
        toks.removeAt(0);
      } else {
        tokenTrimmed = false;
      }
    }

    var res = toks.map((token) {
      if (token.type == "WORD") {
        var existingToken = tokenStore.listOfTokens[token.text.toLowerCase()];
        var color =
            existingToken != null ? existingToken.color : TokenColors.level0;
        var style = textStyle.copyWith(backgroundColor: color);
        return TextSpan(text: token.text, style: style);
      } else {
        return TextSpan(text: token.text, style: textStyle);
      }
    }).toList();

    if (res.isEmpty) res = [];

    return TextSpan(text: '', children: res, style: textStyle);
  }

  Token getWordForPosition(Offset offset) {
    if (textPainter == null) return null;

    var pos = textPainter.getPositionForOffset(offset);
    var wordBounduaries = textPainter.getWordBoundary(pos);

    var ro = page.tokens?.first?.start ?? 0;

    var _ts = page.tokens.where((token) =>
        token.start == wordBounduaries.start + ro &&
        token.end == wordBounduaries.end + ro);

    Token tok;

    if (_ts.isNotEmpty && _ts.first.type == "WORD") tok = _ts.first;

    return tok;
  }
}
