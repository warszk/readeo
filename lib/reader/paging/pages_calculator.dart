import 'package:flutter/widgets.dart';
import 'package:readeo/reader/paging/paging.dart';
import 'package:readeo/reader/models/page.dart' as ReaderModel;
import 'package:readeo/reader/models/page_style.dart';
import 'package:readeo/reader/models/sentence.dart';
import 'package:readeo/reader/models/token.dart';

class PagesCalculator {
  final Size pageSize;
  final PageStyle currentPageStyle;

  PagesCalculator({this.pageSize, this.currentPageStyle});

  Future<List<ReaderModel.Page>> calculatePages(int pos, List<Sentence> sentences) async {
    var _currentLocation = 0;
    var text = sentences.map((s) => s.toText()).join('');
    var txt = text;
    List<ReaderModel.Page> _pgs = List();

    var i = 0;
    while (txt.isNotEmpty) {
      var l = _calculateMaxTextLength(txt);
      List<Token> tokens = List();

      sentences.forEach((sentence) {
        tokens.addAll(sentence.getTokensForLocation(
            _currentLocation, _currentLocation + l));
      });

      if (tokens.length > 0) {
        ReaderModel.Page page = ReaderModel.Page(
            location: ReaderModel.PageLocation(
              start: _currentLocation,
              end: _currentLocation + l,
            ),
            tokens: tokens,
            chapter: pos);

        _pgs.add(page);
      }

      _currentLocation += l;
      txt = text.substring(_currentLocation);
    }

    return _pgs;
  }

  int _calculateMaxTextLength(String txt) {
    Paging paging = Paging(size: pageSize, pageStyle: currentPageStyle);
    paging.layout(txt);

    return paging.maxLength;
  }
}
