import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/reader/ui/chapter_list_dialog.dart';
import 'package:readeo/reader/ui/edit_token_view.dart';
import 'package:readeo/reader/providers/reader_token_store_provider.dart';
import 'package:readeo/reader/ui/unknown_tokens_level_chooser.widget.dart';
import 'package:readeo/reader/models/page.dart' as ReaderModel;
import 'package:readeo/reader/paging/reader_page.widget.dart';
import 'package:readeo/reader/models/token.dart';
import 'package:readeo/reader/providers/reader_provider.dart';
import 'package:readeo/router/reader_nav_arguments.dart';
import 'package:readeo/shared/colors.dart';

class ReaderView extends StatelessWidget {
  final ReaderNavArguments args;
  ReaderView(this.args);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ReaderProvider>(
        create: (BuildContext context) => ReaderProvider(),
        child: _ScaffoldView(args));
  }
}

class _ScaffoldView extends StatelessWidget {
  final ReaderNavArguments args;
  _ScaffoldView(this.args);

  @override
  Widget build(BuildContext context) {
    final readerProvider = Provider.of<ReaderProvider>(context);
    final dbProvider = Provider.of<MyDatabase>(context);
    final tokenStore = Provider.of<ReaderTokenStoreProvider>(context);

    readerProvider.initialize(dbProvider, args.book, tokenStore);

    return Scaffold(
      backgroundColor: ReaderColors.bg,
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: AppColors.primaryColor,
        title: Text(readerProvider.currentChapterName),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add_box),
              onPressed: () {
                showDialog(
                    context: context,
                    child: UnknownTokensLevelChooser(
                      onSave: (int level) {
                        readerProvider.saveUnknownTokens(level);
                      },
                    ));
              }),
          IconButton(
              icon: Icon(Icons.toc),
              onPressed: () {
                showDialog(
                    context: context,
                    child: ChapterListDialog(
                      readerProvider.chapterList,
                      onChapterSelected: (c) => readerProvider.changeChapter(c),
                    ));
              }),
          IconButton(
              icon: Icon(Icons.settings, color: AppColors.secondaryTextColor),
              onPressed: () {}),
        ],
      ),
      body: _ReaderLayoutBuilder(),
    );
  }
}

class _ReaderLayoutBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final readerProvider = Provider.of<ReaderProvider>(context);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraint) {
        readerProvider.pageSize =
            Size(constraint.maxWidth - 40.0, constraint.maxHeight - 40);
        return _PageStreamer();
      },
    );
  }
}

class _PageStreamer extends StatelessWidget {
  Widget build(BuildContext context) {
    final readerProvider = Provider.of<ReaderProvider>(context);

    return StreamBuilder<List<ReaderModel.Page>>(
      stream: readerProvider.pagesStream,
      builder: (context, AsyncSnapshot<List<ReaderModel.Page>> snapshot) {
        if (snapshot.hasData) {
          return _PageBuilder(snapshot.data, readerProvider.getPageByLocation);
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class _PageBuilder extends StatefulWidget {
  final List<ReaderModel.Page> pages;
  final int currentPageNumber;
  _PageBuilder(this.pages, this.currentPageNumber);
  _PageBuilderState createState() => _PageBuilderState(currentPageNumber);
}

class _PageBuilderState extends State<_PageBuilder> {
  PageController _controller;
  int currentPageNumber;

  _PageBuilderState(this.currentPageNumber) {
    _controller = PageController(initialPage: currentPageNumber);
  }

  @override
  Widget build(BuildContext context) {
    final readerProvider = Provider.of<ReaderProvider>(context);

    if (widget.pages.isEmpty) return CircularProgressIndicator();

    return PageView.builder(
      onPageChanged: (page) {
        readerProvider.setCurrentPageNumber(page);
      },
      controller: _controller,
      itemBuilder: (context, position) {
        return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding:
                    EdgeInsets.only(top: 10, bottom: 0, left: 20, right: 20),
                child: ReaderPage(
                  style: readerProvider.currentPageStyle,
                  size: readerProvider.pageSize,
                  page: readerProvider.getPage(position),
                  onTokenTap: (Token token) {
                    showModalBottomSheet(
                        isScrollControlled: true,
                        backgroundColor: AppColors.primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10)),
                        ),
                        context: context,
                        builder: (context) {
                          return EditTokenView(
                              readerProvider.getEditableToken(token),
                              onSave: (tokenSentenceTuple) =>
                                  readerProvider.saveToken(tokenSentenceTuple),
                              onSpeak: (text) => readerProvider.speak(text));
                        });
                  },
                ),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: <Widget>[
                      Divider(),
                      Text(
                          "Chapter progress: ${readerProvider.currentProgress}% (lang: ${readerProvider.book.langCode})",
                          style: TextStyle(
                              fontSize: 12, color: AppColors.secondaryColor))
                    ],
                  ))
            ]);
      },
      // itemCount: widget.chapter.pages.length,
    );
  }
}
