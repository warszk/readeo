import 'package:flutter/material.dart';

class ChapterListDialog extends StatelessWidget {
  final Function onChapterSelected;
  final List<String> list;

  ChapterListDialog(this.list, {this.onChapterSelected});

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text("Chapters"),
      children: list.asMap().entries.map((entry) {
        return SimpleDialogOption(
          child: Text(entry.value),
          onPressed: () {
            if(onChapterSelected != null) onChapterSelected(entry.key);
            Navigator.of(context).pop();
          },
        );
      }).toList(),
    );
  }
}
