import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';

class TranslationDialogView extends StatelessWidget {
  final String translation;
  final String text;

  TranslationDialogView({this.translation, this.text});

  @override
  Widget build(BuildContext context) {
    return CustomDialogWidget(
      title: text,
      icon: Icon(Icons.translate),
      body: Center(
          child: Column(children: [
        SelectableText(translation),
        SizedBox(height: 20),
        ButtonBar(
          children: <Widget>[
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Colors.blue,
              child: Text('Copy'),
              onPressed: () {
                Clipboard.setData(ClipboardData(text: translation));
                Navigator.of(context).pop();
                // Flushbar(
                //   icon: Icon(
                //     Icons.info_outline,
                //     color: Colors.blue,
                //   ),
                //   message: "Translation copied!",
                //   duration: Duration(seconds: 2),
                //   margin: EdgeInsets.all(8),
                //   borderRadius: 8,
                // ).show(context);
              },
            )
          ],
        )
      ])),
    );
  }
}
