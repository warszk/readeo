import 'package:flutter/material.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';

class LevelVal {
  int value = 99;
}

class UnknownTokensLevelChooser extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final Function onSave;
  final LevelVal value = LevelVal();

  UnknownTokensLevelChooser({this.onSave});

  @override
  Widget build(BuildContext context) {
    return CustomDialogWidget(
        title: "Save unknown words",
        icon: Icon(
          Icons.arrow_upward,
          size: 40,
        ),
        body: _UnknownTokensLevelChooser(_formKey, value),
        buttons: ButtonBar(
          children: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.red),
              ),
            ),
            FlatButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  Navigator.of(context).pop();
                  if (onSave != null) {
                    onSave(value.value);
                  }
                }
              },
              child: Text("Save"),
            )
          ],
        ));
  }
}

class _UnknownTokensLevelChooser extends StatefulWidget {
  final GlobalKey<FormState> _formKey;
  final LevelVal value;

  _UnknownTokensLevelChooser(this._formKey, this.value);

  @override
  _UnknownTokensLevelChooserState createState() =>
      _UnknownTokensLevelChooserState();
}

class _UnknownTokensLevelChooserState
    extends State<_UnknownTokensLevelChooser> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget._formKey,
      child: Column(
        children: <Widget>[
          Text("Choose a level"),
          ListTile(
              title: DropdownButton(
            hint: Text("Select level"),
            items: [
              DropdownMenuItem(
                  child: Text('Seen').withLevelRectInfo(TokenColors.level1),
                  value: 1),
              DropdownMenuItem(
                  child: Text('Hardly').withLevelRectInfo(TokenColors.level2),
                  value: 2),
              DropdownMenuItem(
                  child: Text('So-so').withLevelRectInfo(TokenColors.level3),
                  value: 3),
              DropdownMenuItem(
                  child: Text('Good').withLevelRectInfo(TokenColors.level4),
                  value: 4),
              DropdownMenuItem(
                  child:
                      Text('Very good').withLevelRectInfo(TokenColors.level5),
                  value: 5),
              DropdownMenuItem(
                  child: Text('Skipped').withLevelRectInfo(TokenColors.whitey),
                  value: 98),
              DropdownMenuItem(
                  child: Text('Known').withLevelRectInfo(TokenColors.level99),
                  value: 99),
            ],
            value: widget.value.value,
            onChanged: (value) {
              setState(() {
                widget.value.value = value;
              });
            },
            isExpanded: true,
          ))
        ],
      ),
    );
  }
}

extension MyDecoration on Text {
  withLevelRectInfo(Color level) {
    return Row(children: [
      Container(
          width: 10,
          height: 10,
          child: DecoratedBox(decoration: BoxDecoration(color: level))),
      SizedBox(width: 5),
      this
    ]);
  }
}
