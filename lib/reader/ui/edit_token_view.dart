import 'package:flutter/material.dart';
import 'package:readeo/reader/models/token_sentence_tuple.dart';
import 'package:readeo/reader/ui/translation_dialog.dart';
import 'package:readeo/reader/services/translation_service.dart';
import 'package:readeo/shared/colors.dart';

class EditTokenView extends StatefulWidget {
  final TokenSentenceTuple tokenSentenceTuple;
  final Function onSpeak;
  final Function onSave;
  final Function onTranslate;

  EditTokenView(this.tokenSentenceTuple,
      {this.onSave, this.onSpeak, this.onTranslate});

  @override
  _EditTokenViewState createState() => _EditTokenViewState();
}

class _EditTokenViewState extends State<EditTokenView> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 18),
            child: Container(
                child: SingleChildScrollView(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(children: [
                          Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(30)),
                              child: IconButton(
                                icon: Icon(Icons.play_arrow, size: 15),
                                onPressed: () {
                                  if (widget.onSpeak != null)
                                    widget.onSpeak(
                                        widget.tokenSentenceTuple.token.name);
                                },
                              )),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(10)),
                              child: IconButton(
                                icon: Icon(Icons.translate, size: 15),
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return _TranslationDialogFutureBuilder(
                                            widget.tokenSentenceTuple.token.name
                                                .toString(),
                                            widget
                                                .tokenSentenceTuple.langCode2);
                                      });
                                },
                              )),
                          SizedBox(
                            width: 20,
                          ),
                          Flexible(
                              child: SelectableText(
                            widget.tokenSentenceTuple.token.name,
                            style: TextStyle(
                              fontSize: 27.0,
                              fontWeight: FontWeight.bold,
                              color: AppColors.primaryTextColor,
                            ),
                          ))
                        ]),
                        SizedBox(
                          height: 15,
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom),
                            child: TextFormField(
                              initialValue:
                                  widget.tokenSentenceTuple.token.definition,
                              maxLines: null,
                              onChanged: (String _definition) {
                                setState(() {
                                  widget.tokenSentenceTuple.token.definition =
                                      _definition;
                                });
                              },
                              onSaved: (String s) {},
                              style: TextStyle(
                                  fontSize: 18,
                                  color: AppColors.primaryTextColor),
                              decoration: InputDecoration(
                                  labelText: "definition",
                                  labelStyle: TextStyle(
                                      color: AppColors.secondaryTextColor,
                                      fontSize: 14)),
                            )),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level0),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              0
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 0;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level1),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              1
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 1;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level2),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              2
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 2;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level3),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              3
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 3;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level4),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              4
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 4;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level5),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              5
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 5;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level98),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              98
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 98;
                                  });
                                },
                              ),
                              InkWell(
                                splashColor: Colors.red,
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  decoration:
                                      BoxDecoration(color: TokenColors.level99),
                                  child:
                                      (widget.tokenSentenceTuple.token.level ==
                                              99
                                          ? Icon(
                                              Icons.done,
                                              size: 15,
                                            )
                                          : null),
                                ),
                                onTap: () {
                                  setState(() {
                                    widget.tokenSentenceTuple.token.level = 99;
                                  });
                                },
                              ),
                            ]),
                        SizedBox(height: 10),
                        Center(
                            child: Text(
                                "Level: " +
                                    widget.tokenSentenceTuple.token.levelName,
                                style: TextStyle(
                                    fontSize: 16,
                                    color: AppColors.secondaryTextColor))),
                        SizedBox(height: 15),
                        Row(children: [
                          Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(30)),
                              child: IconButton(
                                icon: Icon(Icons.play_arrow, size: 15),
                                onPressed: () {
                                  if (widget.onSpeak != null)
                                    widget.onSpeak(widget
                                        .tokenSentenceTuple.sentence
                                        .toString());
                                },
                              )),
                          SizedBox(width: 20),
                          Flexible(
                              child: SelectableText.rich(
                                  widget.tokenSentenceTuple.sentence.boldWord(
                                      widget.tokenSentenceTuple.locationStart,
                                      TextStyle(
                                          fontSize: 16,
                                          color: AppColors.primaryTextColor)))),
                        ]),
                        SizedBox(
                          height: 15,
                        ),
                        ButtonBar(children: [
                          RaisedButton(
                            color: Colors.green,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return _TranslationDialogFutureBuilder(
                                        widget.tokenSentenceTuple.sentence
                                            .toString(),
                                        widget.tokenSentenceTuple.langCode2);
                                  });
                            },
                            child: Text("Translate sentence"),
                          ),
                          RaisedButton(
                            color: Colors.blue,
                            child: Text("Save"),
                            onPressed: () {
                              if (widget.onSave != null)
                                widget.onSave(widget.tokenSentenceTuple);
                              Navigator.pop(context);
                            },
                          ),
                        ]),
                        SizedBox(height: 5)
                      ],
                    )))));
  }
}

class _TranslationDialogFutureBuilder extends StatelessWidget {
  final String text;
  final String langFrom;

  _TranslationDialogFutureBuilder(this.text, this.langFrom);

  @override
  Widget build(BuildContext context) {
    Locale myLocale = Localizations.localeOf(context);
    return FutureBuilder(
      future: TranslationService()
          .getTranslation(text, langFrom, myLocale.languageCode, "en"),
      builder: (context, AsyncSnapshot<String> snapshot) {
        if (snapshot.hasData) {
          return TranslationDialogView(
            translation: snapshot.data,
            text: text,
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
