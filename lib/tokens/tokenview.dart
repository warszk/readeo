import 'package:flutter/material.dart';
import 'package:readeo/models/token.model.dart';

class TokenView extends StatelessWidget {
  final TokenModel token;

  TokenView(this.token);

  @override
  Widget build(BuildContext context) {
    return _TokenViewScaffold(token);
  }
}

class _TokenViewScaffold extends StatelessWidget {
  final TokenModel entity;

  _TokenViewScaffold(this.entity);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Token"),
      ),
      body: Center(
        child: Text("${entity.name} - ${entity.updatedAt}"),
      ),
    );
  }
}
