import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:csv/csv.dart';
import 'package:moor/moor.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/tokens/filter.dart';
import 'package:readeo/tokens/sort.dart';
import 'package:readeo/models/token.model.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

class TokenProvider {
  final _stream = StreamController<List<TokenModel>>.broadcast();
  Stream get tokensStream => _stream.stream;
  MyDatabase db;
  List<OrderingTerm Function(DbTokens)> _orderBy = [];
  List<Expression<bool, BoolType> Function(DbTokens)> _filters = [];
  Filter _tokensFilter = Filter();
  Sort _tokensSort = Sort();
  StreamSubscription watchListOfTokens;
  List<DbLanguage> languages;

  void setdb(MyDatabase db) async {
    this.db = db;
    languages = await db.languagesDao.getLanguages();
  }

  void listen() {
    if (watchListOfTokens != null && !watchListOfTokens.isPaused) {
      watchListOfTokens.cancel();
    }

    _stream.sink.add(null);

    watchListOfTokens = db.tokensDao
        .watchListOfTokens(order: _orderBy, filters: _filters)
        .listen((data) {
      print(data.length);
      _stream.sink
          .add(data.map((entity) => TokenModel.fromToken(entity)).toList());
    });
  }

  set tokensSort(sort) {
    _tokensSort = sort;
    _sort();
  }

  set tokensFilter(filter) {
    _tokensFilter = filter;
    _filter();
  }

  void _sort() {
    _orderBy.clear();

    if (_tokensSort.updatedAt.include) {
      _orderBy.add((u) => OrderingTerm(
          expression: u.updatedAt, mode: _tokensSort.updatedAt.mode));
    }

    if (_tokensSort.name.include) {
      _orderBy.add(
          (u) => OrderingTerm(expression: u.name, mode: _tokensSort.name.mode));
    }

    if (_tokensSort.language.include) {
      _orderBy.add((u) => OrderingTerm(
          expression: u.languageCode, mode: _tokensSort.language.mode));
    }

    if (_tokensSort.level.include) {
      _orderBy.add((u) =>
          OrderingTerm(expression: u.level, mode: _tokensSort.level.mode));
    }

    listen();
  }

  void _filter() {
    _filters.clear();

    if (_tokensFilter.partialName != null &&
        _tokensFilter.partialName.isNotEmpty) {
      _filters.add((u) => u.name.like("%${_tokensFilter.partialName}%"));
    }

    if (_tokensFilter.language != null) {
      _filters.add((u) => u.languageCode.equals(_tokensFilter.language));
    }

    if (_tokensFilter.level.isNotEmpty) {
      _filters.add((u) => u.level.isIn(_tokensFilter.getLevels()));
    }

    listen();
  }

  void saveNewToken(TokenModel token) {
    token.updatedAt = DateTime.now().toUtc();
    db.tokensDao.upsertToken(token.toDbToken());
  }

  Future<void> exportTokenList() async {
    await requestReadWritePermission();

    var dir = (await getExternalStorageDirectory()).path;
    var tokens = (await db.tokensDao.allTokenEntries)
        .map((t) => [
              t.name,
              t.definition,
              t.name.split(' ').length > 0 ? "1" : "0",
              t.languageCode,
              t.level.toString(),
              t.updatedAt.toString(),
              t.updatedAt ?? 0
            ])
        .toList();

    var data = [
      "phrase",
      "definition",
      "multiphrase",
      "language_code3",
      "level",
      "createdAt",
      "updatedAt"
    ];

    tokens.insert(0, data);

    var outputFileName =
        "$dir/tokens-r-${DateTime.now().toIso8601String()}.itsv";
    print(outputFileName);
    print(tokens.take(20));
    print('---');
    print(tokens.reversed.take(20));
    print('===');

    var output = File(outputFileName);
    await output.create(recursive: true);

    var csv = ListToCsvConverter(eol: "\n", textDelimiter: '"');

    var res = csv.convert(tokens);
    await output.writeAsString(res);
    print("done");
  }

  // temporary solution
  Future<void> importTokenList() async {
    // get the last csv file
    await requestReadWritePermission();

    Directory dir = Directory("/storage/emulated/0/LWT");
    var list = await dir.list().toList();
    var l = list.where((f) => path.extension(f.path) == ".itsv").toList();
    String last;
    int lastModified = 0;

    l.forEach((f) {
      var stats = f.statSync();
      print(stats.modified.millisecondsSinceEpoch);
      print(stats.type);
      print(f.path);
      if (stats.modified.millisecondsSinceEpoch > lastModified) {
        last = f.path;
        lastModified = stats.modified.millisecondsSinceEpoch;
      }
    });

    var input = File(last).openRead();
    var converter = CsvToListConverter(
      eol: "\n",
    );
    final fields =
        await input.transform(utf8.decoder).transform(converter).toList();
    List<DbToken> _toks = List();
    fields.removeAt(0);

    var _i = 0;
    var _curL = fields.length - 1;
    for (var f in fields) {
      print(f);
      var dt = DbToken(
          id: null,
          name: f[0].toString(),
          definition: f[1].toString(),
          languageCode: f[3].toString(),
          level: f[4],
          updatedAt: f[6]);

      _toks.add(dt);
      if (_toks.length > 5000) {
        print("batch ${_i++} - $_curL");

        try {
          await db.tokensDao.upsertAll(_toks);
        } catch (e) {
          print(e);
        }
        _curL -= _toks.length;
        _toks.clear();
      }
    }

    try {
      await db.tokensDao.upsertAll(_toks);
    } catch (e) {
      print(e);
    }
  }

  Future<bool> requestReadWritePermission() async {
    if (await Permission.storage.request().isGranted) {
      return true;
    }

    return false;
  }

  dispose() {
    _stream.close();
    watchListOfTokens.cancel();
  }
}
