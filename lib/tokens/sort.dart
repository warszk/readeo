import 'package:moor/moor.dart';

class SortOption {
  bool include = true;
  OrderingMode mode = OrderingMode.asc;

  SortOption({this.include = true, this.mode = OrderingMode.asc});

  bool get isAsc => mode == OrderingMode.asc;

  setMode(bool m) {
    if(m) {
      this.mode = OrderingMode.asc;
    } else {
      this.mode = OrderingMode.desc;
    }
  }
}

class Sort {
  SortOption updatedAt = SortOption(mode: OrderingMode.desc);
  SortOption name = SortOption(include: false);
  SortOption language = SortOption(include: false);
  SortOption level = SortOption(include: false);
}
