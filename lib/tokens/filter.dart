class Filter {
  String language;
  String partialName;
  List<bool> level = List.filled(8, true, growable: false);

  List<int> getLevels() {
    List<int> l = [];

    level.asMap().forEach((index, value) {
      if(value) {
        int i = index;
        if(i > 5) i=92+i; //98, 99...
        l.add(i);
      }
    });
    return l;
  }
}
