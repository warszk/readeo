
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';
import 'package:readeo/models/token.model.dart';
import 'package:readeo/tokens/token_provider.dart';

class NewTokenWidget extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final TokenModel token = TokenModel();

  @override
  Widget build(BuildContext context) {
    final tokenProvider = Provider.of<TokenProvider>(context);

    return CustomDialogWidget(
      title: "Add new token",
      icon: Icon(Icons.add, size: 40,),
      body: _NewTokenForm(_formKey, token),
      buttons: ButtonBar(
        children: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("Cancel", style: TextStyle(color: Colors.red),),
          ),
          FlatButton(
            onPressed: () async {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                tokenProvider.saveNewToken(token);
                Navigator.of(context).pop();
              }
            },
            child: Text("Save"),
          )
        ],
      )
    );
  }
}

class _NewTokenForm extends StatefulWidget {
  final GlobalKey<FormState> _formKey;
  final TokenModel token;

  _NewTokenForm(this._formKey, this.token);

  @override
  _NewTokenFormState createState() => _NewTokenFormState();
}

class _NewTokenFormState extends State<_NewTokenForm> {
  @override
  Widget build(BuildContext context) {
    final tokenProvider = Provider.of<TokenProvider>(context);

    return Form(
      key: widget._formKey,
      child: Column(
        children: <Widget>[
          ListTile(
            title: TextFormField(
              decoration: InputDecoration(hintText: "Name"),
              validator: (value) {
                if(value.isEmpty) {
                  return 'Please enter some text';
                } else {
                  return null;
                }
              },
              onSaved: (value) {
                setState(() {
                  widget.token.name = value;
                });
              },
            )
          ),
          ListTile(
            title: TextFormField(
              decoration: InputDecoration(hintText: "Description"),
              onSaved: (value) {
                setState(() {
                  widget.token.definition = value;
                });
              },
            ),
          ),
          ListTile(
            title: DropdownButton(
              hint: Text('Select language'),
              items: tokenProvider.languages.map((lang) => DropdownMenuItem(child: Text(lang.name), value: lang.code3),).toList(),
              value: widget.token.langcode,
              isExpanded: true,
              onChanged: (value) {
                setState(() {
                  widget.token.langcode = value;
                });
              },
            )
          ),
          ListTile(
            title: DropdownButton(
              hint: Text("Select level"),
              items: [
                DropdownMenuItem(child: Text('Seen').withLevelRectInfo(TokenColors.level1), value: 1),
                DropdownMenuItem(child: Text('Hardly').withLevelRectInfo(TokenColors.level2), value: 2),
                DropdownMenuItem(child: Text('So-so').withLevelRectInfo(TokenColors.level3), value: 3),
                DropdownMenuItem(child: Text('Good').withLevelRectInfo(TokenColors.level4), value: 4),
                DropdownMenuItem(child: Text('Very good').withLevelRectInfo(TokenColors.level5), value: 5),
                DropdownMenuItem(child: Text('Skipped').withLevelRectInfo(TokenColors.whitey), value: 98),
                DropdownMenuItem(child: Text('Known').withLevelRectInfo(TokenColors.level99), value: 99),
              ],
              value: widget.token.level,
              onChanged: (value) {
                setState(() {
                  widget.token.level = value;
                });
              },
              isExpanded: true,
            )
          )
        ],
      ),
    );
  }
}

extension MyDecoration on Text {
  withLevelRectInfo(Color level) {
    return Row(
      children: [
        Container(width: 10, height: 10, child: DecoratedBox(decoration: BoxDecoration(color: level))),
        SizedBox(width: 5),
        this
      ]
    );
  }
}
