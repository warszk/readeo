import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';
import 'package:readeo/tokens/sort.dart';
import 'package:readeo/tokens/token_provider.dart';

class SortWidget extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final Sort tokensSort = Sort();

  @override
  Widget build(BuildContext context) {
    TokenProvider tokenProvider = Provider.of<TokenProvider>(context);

    return CustomDialogWidget(
      title: "Sort by",
      icon: Icon(Icons.sort, size: 40,),
      body: _SortTokensForm(_formKey, tokensSort),
      buttons: ButtonBar(
        children: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("Cancel", style: TextStyle(color: Colors.red),),
          ),
          FlatButton(
            onPressed: () async {
              _formKey.currentState.save();
              tokenProvider.tokensSort = tokensSort;
              Navigator.of(context).pop();
            },
            child: Text("Sort"),
          )
        ],
      )
    );
  }
}


class _SortTokensForm extends StatefulWidget {
  final GlobalKey<FormState> _formKey;
  final Sort tokensSort;

  _SortTokensForm(this._formKey, this.tokensSort);

  @override
  _SortTokensFormState createState() => _SortTokensFormState();
}

class _SortTokensFormState extends State<_SortTokensForm> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget._formKey,
      child: Column(
        children: <Widget>[
          ListTile(
            title: Row(
              children: [
                Checkbox(
                  value: widget.tokensSort.updatedAt.include,
                  onChanged: (val) {
                    setState(() {
                      widget.tokensSort.updatedAt.include = val;
                    });
                  },
                ),
                Text("Update time"),
                Switch(
                  value: widget.tokensSort.updatedAt.isAsc,
                  onChanged: (value) {
                    setState(() {
                      widget.tokensSort.updatedAt.setMode(value);
                    });
                  },
                )
              ]
            )
          ),
          ListTile(
            title: Row(
              children: [
                Checkbox(
                  value: widget.tokensSort.name.include,
                  onChanged: (val) {
                    setState(() {
                      widget.tokensSort.name.include = val;
                    });
                  },
                ),
                Text("Name"),
                Switch(
                  value: widget.tokensSort.name.isAsc,
                  onChanged: (value) {
                    setState(() {
                      widget.tokensSort.name.setMode(value);
                    });
                  },
                )
              ]
            )
          ),
          ListTile(
            title: Row(
              children: [
                Checkbox(
                  value: widget.tokensSort.language.include,
                  onChanged: (val) {
                    setState(() {
                      widget.tokensSort.language.include = val;
                    });
                  },
                ),
                Text("Language"),
                Switch(
                  value: widget.tokensSort.language.isAsc,
                  onChanged: (value) {
                    setState(() {
                      widget.tokensSort.language.setMode(value);
                    });
                  },
                )
              ]
            )
          ),
          ListTile(
            title: Row(
              children: [
                Checkbox(
                  value: widget.tokensSort.level.include,
                  onChanged: (val) {
                    setState(() {
                      widget.tokensSort.level.include = val;
                    });
                  },
                ),
                Text("Level"),
                Switch(
                  value: widget.tokensSort.level.isAsc,
                  onChanged: (value) {
                    setState(() {
                      widget.tokensSort.level.setMode(value);
                    });
                  },
                )
              ]
            )
          ),
        ],
      ),
    );
  }
}
