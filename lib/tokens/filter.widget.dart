
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/shared/custom_checkbox.widget.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';
import 'package:readeo/tokens/filter.dart';
import 'package:readeo/tokens/token_provider.dart';

class FilterWidget extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final Filter filter = Filter();

  @override
  Widget build(BuildContext context) {
    TokenProvider tokenProvider = Provider.of<TokenProvider>(context);

    return CustomDialogWidget(
      title: "Filter",
      icon: Icon(Icons.filter_list, size: 40,),
      body: _FilterTokensForm(_formKey, filter),
      buttons: ButtonBar(
        children: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("Cancel", style: TextStyle(color: Colors.red),),
          ),
          FlatButton(
            onPressed: () {

              Navigator.of(context).pop();
            },
            child: Text("Clear"),
          ),
          FlatButton(
            onPressed: () async {
              _formKey.currentState.save();
              tokenProvider.tokensFilter = filter;
              Navigator.of(context).pop();
            },
            child: Text("Filter"),
          )
        ],
      ),
    );
  }
}

class _FilterTokensForm extends StatefulWidget {
  final GlobalKey<FormState> _formKey;
  final Filter filter;

  _FilterTokensForm(this._formKey, this.filter);

  @override
  _FilterTokensFormState createState() => _FilterTokensFormState();
}

class _FilterTokensFormState extends State<_FilterTokensForm> {
  List<DropdownMenuItem> languageList(List<DbLanguage> languages) {
    return languages.map((lang) =>
        DropdownMenuItem(child: Text(lang.name), value: lang.code3)
    ).toList()
    ..insert(0, DropdownMenuItem(child: Text('All languages'), value: null,));
  }


  @override
  Widget build(BuildContext context) {
    TokenProvider tokenProvider = Provider.of<TokenProvider>(context);

    return Form(
      key: widget._formKey,
      child: Column(
        children: <Widget>[
          ListTile(
            title: TextFormField(
              initialValue: widget.filter.partialName,
              decoration: InputDecoration(hintText: "Partial name"),
              onSaved: (value) {
                setState(() {
                  widget.filter.partialName = value;
                });
              },
            )
          ),
          ListTile(
            title: DropdownButton(
              hint: Text("Select language"),
              items: languageList(tokenProvider.languages),
              value: widget.filter.language,
              onChanged: (value) {
                setState(() {
                  widget.filter.language = value;
                });
              },
              isExpanded: true,
            )
          ),
          ListTile(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Levels"),
                SizedBox(height: 3),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CustomCheckbox(
                      value: widget.filter.level[0],
                      activeColor: TokenColors.level0,
                      borderColor: TokenColors.level0,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[0] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[1],
                      activeColor: TokenColors.level1,
                      borderColor: TokenColors.level1,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[1] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[2],
                      activeColor: TokenColors.level2,
                      borderColor: TokenColors.level2,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[2] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[3],
                      activeColor: TokenColors.level3,
                      borderColor: TokenColors.level3,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[3] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[4],
                      activeColor: TokenColors.level4,
                      borderColor: TokenColors.level4,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[4] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[5],
                      activeColor: TokenColors.level5,
                      borderColor: TokenColors.level5,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[5] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[6],
                      activeColor: TokenColors.whitey,
                      borderColor: TokenColors.whitey,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[6] = val;
                        });
                      },
                    ),
                    CustomCheckbox(
                      tristate: false,
                      value: widget.filter.level[7],
                      activeColor: TokenColors.level99,
                      borderColor: TokenColors.level99,
                      onChanged: (val) {
                        setState(() {
                          widget.filter.level[7] = val;
                        });
                      },
                    )
                  ],
                )
            ],
          ))
        ],
      ),
    );
  }
}
