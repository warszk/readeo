import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/tokens/filter.widget.dart';
import 'package:readeo/tokens/new_token.widget.dart';
import 'package:readeo/tokens/sort.widget.dart';
import 'package:readeo/models/token.model.dart';
import 'package:readeo/tokens/token_provider.dart';
import 'package:readeo/tokens/tokenview.dart';

class TokenListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<TokenProvider>(
      create: (BuildContext context) => TokenProvider(),
      child: _ScaffoldView(),
      dispose: (BuildContext context, TokenProvider provider) =>
          provider.dispose(),
    );
  }
}

class _ScaffoldView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tokenProvider = Provider.of<TokenProvider>(context);
    final databaseProvider = Provider.of<MyDatabase>(context);
    tokenProvider.setdb(databaseProvider);
    tokenProvider.listen();

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: AppColors.primaryColor,
        title: Text("Tokens"),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (item) async {
              switch (item) {
                case "import":
                  await tokenProvider.importTokenList();
                  break;
                case "export":
                  await tokenProvider.exportTokenList();
                  break;
                default:
                  break;
              }
            },
            itemBuilder: (context) => <PopupMenuEntry<String>>[
              PopupMenuItem(value: "import", child: Text("Import")),
              PopupMenuItem(value: "export", child: Text("Export")),
            ],
          ),
        ],
      ),
      body: _TokenListStreamWidget(),
      bottomNavigationBar: Container(
        height: 55.0,
        child: BottomAppBar(
          color: AppColors.primaryColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.home, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              IconButton(
                icon: Icon(Icons.filter_list, color: Colors.white),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => FilterWidget());
                },
              ),
              IconButton(
                icon: Icon(Icons.sort, color: Colors.white),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => SortWidget());
                },
              ),
              IconButton(
                icon: Icon(Icons.add_box, color: Colors.white),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => NewTokenWidget());
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _TokenListStreamWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tokenProvider = Provider.of<TokenProvider>(context);
    return StreamBuilder<List<TokenModel>>(
      stream: tokenProvider.tokensStream,
      builder: (context, AsyncSnapshot<List<TokenModel>> snapshot) {
        if (snapshot.hasData) {
          return _TokenListViewBuilder(snapshot.data);
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class _TokenListViewBuilder extends StatelessWidget {
  final List<TokenModel> tokens;
  _TokenListViewBuilder(this.tokens);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: tokens.length,
      itemBuilder: (context, index) {
        return Card(
          elevation: 8.0,
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: AppColors.secondaryColor),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              leading: Container(
                  padding: EdgeInsets.only(right: 12),
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide(
                              width: 1.0,
                              color: AppColors.secondaryTextColor))),
                  child: Column(children: [
                    Text(
                      tokens[index].langcode,
                      style: TextStyle(color: AppColors.secondaryTextColor),
                    ),
                    SizedBox(height: 10),
                    Icon(Icons.fiber_manual_record, color: tokens[index].color),
                  ])),
              title: Text(tokens[index].name,
                  style: TextStyle(
                      color: AppColors.primaryTextColor,
                      fontWeight: FontWeight.bold)),
              subtitle: Text(tokens[index].definition,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: AppColors.secondaryTextColor)),
              trailing: Icon(Icons.keyboard_arrow_right,
                  color: AppColors.primaryTextColor, size: 30.0),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TokenView(tokens[index])));
              },
            ),
          ),
        );
      },
    );
  }
}
