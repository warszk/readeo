import 'package:moor/moor.dart';
import 'package:readeo/db/database.dart';

part 'tokens.dao.g.dart';

@UseDao(tables: [DbTokens])
class TokensDao extends DatabaseAccessor<MyDatabase> with _$TokensDaoMixin {
  TokensDao(MyDatabase db) : super(db);

  Future<DbToken> findToken(String name, String languageCode) async {
    var query = select(db.dbTokens);
    query.where((u) => u.name.equals(name));
    query.where((u) => u.languageCode.equals(languageCode));

    return await query.getSingle();
  }

  Future<int> upsertToken(DbToken token) async {
    if(token.id != null) {
      await update(db.dbTokens).replace(token);
      return token.id;
    } else {
      return into(db.dbTokens).insert(token);
    }
  }

  Future<List<DbToken>> get allTokenEntries => select(db.dbTokens).get();

  Stream<List<DbToken>> watchListOfTokens({List<OrderingTerm Function(DbTokens)> order, List<Expression<bool, BoolType> Function(DbTokens)> filters}) {
    var _select = select(db.dbTokens);
    if(order != null && order.isNotEmpty) {
      _select.orderBy(order);
    }

    if(filters != null && filters.isNotEmpty) {
      filters.forEach((f) => _select.where(f));
    }

    return (_select).watch();
  }

  Future<void> upsertAll(List<DbToken> tokens) async {
    return await batch((t) {
      t.insertAll(db.dbTokens, tokens);
    });
  }
}
