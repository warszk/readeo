import 'dart:io';
import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:readeo/db/books.dao.dart';
import 'package:readeo/db/languages.dao.dart';
import 'package:readeo/db/tokens.dao.dart';

part "database.g.dart";

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return VmDatabase(file);
  });
}


@UseMoor(include: {'sql.moor'}, daos: [TokensDao, LanguagesDao, BooksDao])
class MyDatabase extends _$MyDatabase {
  MyDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
    onCreate: (Migrator m) {
      return m.createAll();
    },
    onUpgrade: (Migrator m, int from, int to) async {
      if (from == 1) {
        // :)
      }
    },
    beforeOpen: (details) async {
      if(details.wasCreated) {
        await into(dbLanguages).insert(DbLanguage(id: null, name: "español", code2: "es", code3: "spa"));
        await into(dbLanguages).insert(DbLanguage(id: null, name: "english", code2: "en", code3: "eng"));
        await into(dbLanguages).insert(DbLanguage(id: null, name: "русский", code2: "ru", code3: "rus"));
        await into(dbLanguages).insert(DbLanguage(id: null, name: "deutsch", code2: "de", code3: "deu"));
        await into(dbLanguages).insert(DbLanguage(id: null, name: "polski", code2: "pl", code3: "pol"));
      }

      await customStatement('PRAGMA foreign_keys = ON');
    }
  );
}
