// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class DbToken extends DataClass implements Insertable<DbToken> {
  final int id;
  final String name;
  final String definition;
  final int level;
  final int updatedAt;
  final String languageCode;
  DbToken(
      {@required this.id,
      @required this.name,
      this.definition,
      this.level,
      this.updatedAt,
      @required this.languageCode});
  factory DbToken.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return DbToken(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      definition: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}definition']),
      level: intType.mapFromDatabaseResponse(data['${effectivePrefix}level']),
      updatedAt:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}updatedAt']),
      languageCode: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}languageCode']),
    );
  }
  factory DbToken.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbToken(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      definition: serializer.fromJson<String>(json['definition']),
      level: serializer.fromJson<int>(json['level']),
      updatedAt: serializer.fromJson<int>(json['updatedAt']),
      languageCode: serializer.fromJson<String>(json['languageCode']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'definition': serializer.toJson<String>(definition),
      'level': serializer.toJson<int>(level),
      'updatedAt': serializer.toJson<int>(updatedAt),
      'languageCode': serializer.toJson<String>(languageCode),
    };
  }

  @override
  DbTokensCompanion createCompanion(bool nullToAbsent) {
    return DbTokensCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      definition: definition == null && nullToAbsent
          ? const Value.absent()
          : Value(definition),
      level:
          level == null && nullToAbsent ? const Value.absent() : Value(level),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
      languageCode: languageCode == null && nullToAbsent
          ? const Value.absent()
          : Value(languageCode),
    );
  }

  DbToken copyWith(
          {int id,
          String name,
          String definition,
          int level,
          int updatedAt,
          String languageCode}) =>
      DbToken(
        id: id ?? this.id,
        name: name ?? this.name,
        definition: definition ?? this.definition,
        level: level ?? this.level,
        updatedAt: updatedAt ?? this.updatedAt,
        languageCode: languageCode ?? this.languageCode,
      );
  @override
  String toString() {
    return (StringBuffer('DbToken(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('definition: $definition, ')
          ..write('level: $level, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('languageCode: $languageCode')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(
              definition.hashCode,
              $mrjc(level.hashCode,
                  $mrjc(updatedAt.hashCode, languageCode.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DbToken &&
          other.id == this.id &&
          other.name == this.name &&
          other.definition == this.definition &&
          other.level == this.level &&
          other.updatedAt == this.updatedAt &&
          other.languageCode == this.languageCode);
}

class DbTokensCompanion extends UpdateCompanion<DbToken> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> definition;
  final Value<int> level;
  final Value<int> updatedAt;
  final Value<String> languageCode;
  const DbTokensCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.definition = const Value.absent(),
    this.level = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.languageCode = const Value.absent(),
  });
  DbTokensCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    this.definition = const Value.absent(),
    this.level = const Value.absent(),
    this.updatedAt = const Value.absent(),
    @required String languageCode,
  })  : name = Value(name),
        languageCode = Value(languageCode);
  DbTokensCompanion copyWith(
      {Value<int> id,
      Value<String> name,
      Value<String> definition,
      Value<int> level,
      Value<int> updatedAt,
      Value<String> languageCode}) {
    return DbTokensCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      definition: definition ?? this.definition,
      level: level ?? this.level,
      updatedAt: updatedAt ?? this.updatedAt,
      languageCode: languageCode ?? this.languageCode,
    );
  }
}

class DbTokens extends Table with TableInfo<DbTokens, DbToken> {
  final GeneratedDatabase _db;
  final String _alias;
  DbTokens(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _definitionMeta = const VerificationMeta('definition');
  GeneratedTextColumn _definition;
  GeneratedTextColumn get definition => _definition ??= _constructDefinition();
  GeneratedTextColumn _constructDefinition() {
    return GeneratedTextColumn('definition', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _levelMeta = const VerificationMeta('level');
  GeneratedIntColumn _level;
  GeneratedIntColumn get level => _level ??= _constructLevel();
  GeneratedIntColumn _constructLevel() {
    return GeneratedIntColumn('level', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedIntColumn _updatedAt;
  GeneratedIntColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedIntColumn _constructUpdatedAt() {
    return GeneratedIntColumn('updatedAt', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _languageCodeMeta =
      const VerificationMeta('languageCode');
  GeneratedTextColumn _languageCode;
  GeneratedTextColumn get languageCode =>
      _languageCode ??= _constructLanguageCode();
  GeneratedTextColumn _constructLanguageCode() {
    return GeneratedTextColumn('languageCode', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, name, definition, level, updatedAt, languageCode];
  @override
  DbTokens get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'dbTokens';
  @override
  final String actualTableName = 'dbTokens';
  @override
  VerificationContext validateIntegrity(DbTokensCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.definition.present) {
      context.handle(_definitionMeta,
          definition.isAcceptableValue(d.definition.value, _definitionMeta));
    }
    if (d.level.present) {
      context.handle(
          _levelMeta, level.isAcceptableValue(d.level.value, _levelMeta));
    }
    if (d.updatedAt.present) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableValue(d.updatedAt.value, _updatedAtMeta));
    }
    if (d.languageCode.present) {
      context.handle(
          _languageCodeMeta,
          languageCode.isAcceptableValue(
              d.languageCode.value, _languageCodeMeta));
    } else if (isInserting) {
      context.missing(_languageCodeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbToken map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DbToken.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DbTokensCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.definition.present) {
      map['definition'] = Variable<String, StringType>(d.definition.value);
    }
    if (d.level.present) {
      map['level'] = Variable<int, IntType>(d.level.value);
    }
    if (d.updatedAt.present) {
      map['updatedAt'] = Variable<int, IntType>(d.updatedAt.value);
    }
    if (d.languageCode.present) {
      map['languageCode'] = Variable<String, StringType>(d.languageCode.value);
    }
    return map;
  }

  @override
  DbTokens createAlias(String alias) {
    return DbTokens(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class DbLanguage extends DataClass implements Insertable<DbLanguage> {
  final int id;
  final String name;
  final String code2;
  final String code3;
  final String ttsEngine;
  final String ttsVoice;
  DbLanguage(
      {@required this.id,
      @required this.name,
      this.code2,
      @required this.code3,
      this.ttsEngine,
      this.ttsVoice});
  factory DbLanguage.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return DbLanguage(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      code2:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}code2']),
      code3:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}code3']),
      ttsEngine: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}ttsEngine']),
      ttsVoice: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}ttsVoice']),
    );
  }
  factory DbLanguage.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbLanguage(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      code2: serializer.fromJson<String>(json['code2']),
      code3: serializer.fromJson<String>(json['code3']),
      ttsEngine: serializer.fromJson<String>(json['ttsEngine']),
      ttsVoice: serializer.fromJson<String>(json['ttsVoice']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'code2': serializer.toJson<String>(code2),
      'code3': serializer.toJson<String>(code3),
      'ttsEngine': serializer.toJson<String>(ttsEngine),
      'ttsVoice': serializer.toJson<String>(ttsVoice),
    };
  }

  @override
  DbLanguagesCompanion createCompanion(bool nullToAbsent) {
    return DbLanguagesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      code2:
          code2 == null && nullToAbsent ? const Value.absent() : Value(code2),
      code3:
          code3 == null && nullToAbsent ? const Value.absent() : Value(code3),
      ttsEngine: ttsEngine == null && nullToAbsent
          ? const Value.absent()
          : Value(ttsEngine),
      ttsVoice: ttsVoice == null && nullToAbsent
          ? const Value.absent()
          : Value(ttsVoice),
    );
  }

  DbLanguage copyWith(
          {int id,
          String name,
          String code2,
          String code3,
          String ttsEngine,
          String ttsVoice}) =>
      DbLanguage(
        id: id ?? this.id,
        name: name ?? this.name,
        code2: code2 ?? this.code2,
        code3: code3 ?? this.code3,
        ttsEngine: ttsEngine ?? this.ttsEngine,
        ttsVoice: ttsVoice ?? this.ttsVoice,
      );
  @override
  String toString() {
    return (StringBuffer('DbLanguage(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('code2: $code2, ')
          ..write('code3: $code3, ')
          ..write('ttsEngine: $ttsEngine, ')
          ..write('ttsVoice: $ttsVoice')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(
              code2.hashCode,
              $mrjc(code3.hashCode,
                  $mrjc(ttsEngine.hashCode, ttsVoice.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DbLanguage &&
          other.id == this.id &&
          other.name == this.name &&
          other.code2 == this.code2 &&
          other.code3 == this.code3 &&
          other.ttsEngine == this.ttsEngine &&
          other.ttsVoice == this.ttsVoice);
}

class DbLanguagesCompanion extends UpdateCompanion<DbLanguage> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> code2;
  final Value<String> code3;
  final Value<String> ttsEngine;
  final Value<String> ttsVoice;
  const DbLanguagesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.code2 = const Value.absent(),
    this.code3 = const Value.absent(),
    this.ttsEngine = const Value.absent(),
    this.ttsVoice = const Value.absent(),
  });
  DbLanguagesCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    this.code2 = const Value.absent(),
    @required String code3,
    this.ttsEngine = const Value.absent(),
    this.ttsVoice = const Value.absent(),
  })  : name = Value(name),
        code3 = Value(code3);
  DbLanguagesCompanion copyWith(
      {Value<int> id,
      Value<String> name,
      Value<String> code2,
      Value<String> code3,
      Value<String> ttsEngine,
      Value<String> ttsVoice}) {
    return DbLanguagesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      code2: code2 ?? this.code2,
      code3: code3 ?? this.code3,
      ttsEngine: ttsEngine ?? this.ttsEngine,
      ttsVoice: ttsVoice ?? this.ttsVoice,
    );
  }
}

class DbLanguages extends Table with TableInfo<DbLanguages, DbLanguage> {
  final GeneratedDatabase _db;
  final String _alias;
  DbLanguages(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _code2Meta = const VerificationMeta('code2');
  GeneratedTextColumn _code2;
  GeneratedTextColumn get code2 => _code2 ??= _constructCode2();
  GeneratedTextColumn _constructCode2() {
    return GeneratedTextColumn('code2', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _code3Meta = const VerificationMeta('code3');
  GeneratedTextColumn _code3;
  GeneratedTextColumn get code3 => _code3 ??= _constructCode3();
  GeneratedTextColumn _constructCode3() {
    return GeneratedTextColumn('code3', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _ttsEngineMeta = const VerificationMeta('ttsEngine');
  GeneratedTextColumn _ttsEngine;
  GeneratedTextColumn get ttsEngine => _ttsEngine ??= _constructTtsEngine();
  GeneratedTextColumn _constructTtsEngine() {
    return GeneratedTextColumn('ttsEngine', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _ttsVoiceMeta = const VerificationMeta('ttsVoice');
  GeneratedTextColumn _ttsVoice;
  GeneratedTextColumn get ttsVoice => _ttsVoice ??= _constructTtsVoice();
  GeneratedTextColumn _constructTtsVoice() {
    return GeneratedTextColumn('ttsVoice', $tableName, true,
        $customConstraints: '');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, name, code2, code3, ttsEngine, ttsVoice];
  @override
  DbLanguages get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'dbLanguages';
  @override
  final String actualTableName = 'dbLanguages';
  @override
  VerificationContext validateIntegrity(DbLanguagesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.code2.present) {
      context.handle(
          _code2Meta, code2.isAcceptableValue(d.code2.value, _code2Meta));
    }
    if (d.code3.present) {
      context.handle(
          _code3Meta, code3.isAcceptableValue(d.code3.value, _code3Meta));
    } else if (isInserting) {
      context.missing(_code3Meta);
    }
    if (d.ttsEngine.present) {
      context.handle(_ttsEngineMeta,
          ttsEngine.isAcceptableValue(d.ttsEngine.value, _ttsEngineMeta));
    }
    if (d.ttsVoice.present) {
      context.handle(_ttsVoiceMeta,
          ttsVoice.isAcceptableValue(d.ttsVoice.value, _ttsVoiceMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbLanguage map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DbLanguage.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DbLanguagesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.code2.present) {
      map['code2'] = Variable<String, StringType>(d.code2.value);
    }
    if (d.code3.present) {
      map['code3'] = Variable<String, StringType>(d.code3.value);
    }
    if (d.ttsEngine.present) {
      map['ttsEngine'] = Variable<String, StringType>(d.ttsEngine.value);
    }
    if (d.ttsVoice.present) {
      map['ttsVoice'] = Variable<String, StringType>(d.ttsVoice.value);
    }
    return map;
  }

  @override
  DbLanguages createAlias(String alias) {
    return DbLanguages(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class DbBook extends DataClass implements Insertable<DbBook> {
  final int id;
  final String uuid;
  final String title;
  final String authors;
  final String status;
  final String type;
  final String description;
  final int totalByteLength;
  final String languageCode;
  final int lastSeenChapter;
  final int lastSeenPositionInChapter;
  final int updatedAt;
  DbBook(
      {@required this.id,
      @required this.uuid,
      @required this.title,
      this.authors,
      @required this.status,
      @required this.type,
      this.description,
      this.totalByteLength,
      @required this.languageCode,
      this.lastSeenChapter,
      this.lastSeenPositionInChapter,
      this.updatedAt});
  factory DbBook.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return DbBook(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      uuid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uuid']),
      title:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}title']),
      authors:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}authors']),
      status:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}status']),
      type: stringType.mapFromDatabaseResponse(data['${effectivePrefix}type']),
      description: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
      totalByteLength: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}totalByteLength']),
      languageCode: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}languageCode']),
      lastSeenChapter: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}lastSeenChapter']),
      lastSeenPositionInChapter: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}lastSeenPositionInChapter']),
      updatedAt:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}updatedAt']),
    );
  }
  factory DbBook.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbBook(
      id: serializer.fromJson<int>(json['id']),
      uuid: serializer.fromJson<String>(json['uuid']),
      title: serializer.fromJson<String>(json['title']),
      authors: serializer.fromJson<String>(json['authors']),
      status: serializer.fromJson<String>(json['status']),
      type: serializer.fromJson<String>(json['type']),
      description: serializer.fromJson<String>(json['description']),
      totalByteLength: serializer.fromJson<int>(json['totalByteLength']),
      languageCode: serializer.fromJson<String>(json['languageCode']),
      lastSeenChapter: serializer.fromJson<int>(json['lastSeenChapter']),
      lastSeenPositionInChapter:
          serializer.fromJson<int>(json['lastSeenPositionInChapter']),
      updatedAt: serializer.fromJson<int>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'uuid': serializer.toJson<String>(uuid),
      'title': serializer.toJson<String>(title),
      'authors': serializer.toJson<String>(authors),
      'status': serializer.toJson<String>(status),
      'type': serializer.toJson<String>(type),
      'description': serializer.toJson<String>(description),
      'totalByteLength': serializer.toJson<int>(totalByteLength),
      'languageCode': serializer.toJson<String>(languageCode),
      'lastSeenChapter': serializer.toJson<int>(lastSeenChapter),
      'lastSeenPositionInChapter':
          serializer.toJson<int>(lastSeenPositionInChapter),
      'updatedAt': serializer.toJson<int>(updatedAt),
    };
  }

  @override
  DbBooksCompanion createCompanion(bool nullToAbsent) {
    return DbBooksCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      authors: authors == null && nullToAbsent
          ? const Value.absent()
          : Value(authors),
      status:
          status == null && nullToAbsent ? const Value.absent() : Value(status),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      totalByteLength: totalByteLength == null && nullToAbsent
          ? const Value.absent()
          : Value(totalByteLength),
      languageCode: languageCode == null && nullToAbsent
          ? const Value.absent()
          : Value(languageCode),
      lastSeenChapter: lastSeenChapter == null && nullToAbsent
          ? const Value.absent()
          : Value(lastSeenChapter),
      lastSeenPositionInChapter:
          lastSeenPositionInChapter == null && nullToAbsent
              ? const Value.absent()
              : Value(lastSeenPositionInChapter),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  DbBook copyWith(
          {int id,
          String uuid,
          String title,
          String authors,
          String status,
          String type,
          String description,
          int totalByteLength,
          String languageCode,
          int lastSeenChapter,
          int lastSeenPositionInChapter,
          int updatedAt}) =>
      DbBook(
        id: id ?? this.id,
        uuid: uuid ?? this.uuid,
        title: title ?? this.title,
        authors: authors ?? this.authors,
        status: status ?? this.status,
        type: type ?? this.type,
        description: description ?? this.description,
        totalByteLength: totalByteLength ?? this.totalByteLength,
        languageCode: languageCode ?? this.languageCode,
        lastSeenChapter: lastSeenChapter ?? this.lastSeenChapter,
        lastSeenPositionInChapter:
            lastSeenPositionInChapter ?? this.lastSeenPositionInChapter,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('DbBook(')
          ..write('id: $id, ')
          ..write('uuid: $uuid, ')
          ..write('title: $title, ')
          ..write('authors: $authors, ')
          ..write('status: $status, ')
          ..write('type: $type, ')
          ..write('description: $description, ')
          ..write('totalByteLength: $totalByteLength, ')
          ..write('languageCode: $languageCode, ')
          ..write('lastSeenChapter: $lastSeenChapter, ')
          ..write('lastSeenPositionInChapter: $lastSeenPositionInChapter, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          uuid.hashCode,
          $mrjc(
              title.hashCode,
              $mrjc(
                  authors.hashCode,
                  $mrjc(
                      status.hashCode,
                      $mrjc(
                          type.hashCode,
                          $mrjc(
                              description.hashCode,
                              $mrjc(
                                  totalByteLength.hashCode,
                                  $mrjc(
                                      languageCode.hashCode,
                                      $mrjc(
                                          lastSeenChapter.hashCode,
                                          $mrjc(
                                              lastSeenPositionInChapter
                                                  .hashCode,
                                              updatedAt.hashCode))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DbBook &&
          other.id == this.id &&
          other.uuid == this.uuid &&
          other.title == this.title &&
          other.authors == this.authors &&
          other.status == this.status &&
          other.type == this.type &&
          other.description == this.description &&
          other.totalByteLength == this.totalByteLength &&
          other.languageCode == this.languageCode &&
          other.lastSeenChapter == this.lastSeenChapter &&
          other.lastSeenPositionInChapter == this.lastSeenPositionInChapter &&
          other.updatedAt == this.updatedAt);
}

class DbBooksCompanion extends UpdateCompanion<DbBook> {
  final Value<int> id;
  final Value<String> uuid;
  final Value<String> title;
  final Value<String> authors;
  final Value<String> status;
  final Value<String> type;
  final Value<String> description;
  final Value<int> totalByteLength;
  final Value<String> languageCode;
  final Value<int> lastSeenChapter;
  final Value<int> lastSeenPositionInChapter;
  final Value<int> updatedAt;
  const DbBooksCompanion({
    this.id = const Value.absent(),
    this.uuid = const Value.absent(),
    this.title = const Value.absent(),
    this.authors = const Value.absent(),
    this.status = const Value.absent(),
    this.type = const Value.absent(),
    this.description = const Value.absent(),
    this.totalByteLength = const Value.absent(),
    this.languageCode = const Value.absent(),
    this.lastSeenChapter = const Value.absent(),
    this.lastSeenPositionInChapter = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  DbBooksCompanion.insert({
    this.id = const Value.absent(),
    @required String uuid,
    @required String title,
    this.authors = const Value.absent(),
    @required String status,
    @required String type,
    this.description = const Value.absent(),
    this.totalByteLength = const Value.absent(),
    @required String languageCode,
    this.lastSeenChapter = const Value.absent(),
    this.lastSeenPositionInChapter = const Value.absent(),
    this.updatedAt = const Value.absent(),
  })  : uuid = Value(uuid),
        title = Value(title),
        status = Value(status),
        type = Value(type),
        languageCode = Value(languageCode);
  DbBooksCompanion copyWith(
      {Value<int> id,
      Value<String> uuid,
      Value<String> title,
      Value<String> authors,
      Value<String> status,
      Value<String> type,
      Value<String> description,
      Value<int> totalByteLength,
      Value<String> languageCode,
      Value<int> lastSeenChapter,
      Value<int> lastSeenPositionInChapter,
      Value<int> updatedAt}) {
    return DbBooksCompanion(
      id: id ?? this.id,
      uuid: uuid ?? this.uuid,
      title: title ?? this.title,
      authors: authors ?? this.authors,
      status: status ?? this.status,
      type: type ?? this.type,
      description: description ?? this.description,
      totalByteLength: totalByteLength ?? this.totalByteLength,
      languageCode: languageCode ?? this.languageCode,
      lastSeenChapter: lastSeenChapter ?? this.lastSeenChapter,
      lastSeenPositionInChapter:
          lastSeenPositionInChapter ?? this.lastSeenPositionInChapter,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }
}

class DbBooks extends Table with TableInfo<DbBooks, DbBook> {
  final GeneratedDatabase _db;
  final String _alias;
  DbBooks(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  GeneratedTextColumn _uuid;
  GeneratedTextColumn get uuid => _uuid ??= _constructUuid();
  GeneratedTextColumn _constructUuid() {
    return GeneratedTextColumn('uuid', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _titleMeta = const VerificationMeta('title');
  GeneratedTextColumn _title;
  GeneratedTextColumn get title => _title ??= _constructTitle();
  GeneratedTextColumn _constructTitle() {
    return GeneratedTextColumn('title', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _authorsMeta = const VerificationMeta('authors');
  GeneratedTextColumn _authors;
  GeneratedTextColumn get authors => _authors ??= _constructAuthors();
  GeneratedTextColumn _constructAuthors() {
    return GeneratedTextColumn('authors', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _statusMeta = const VerificationMeta('status');
  GeneratedTextColumn _status;
  GeneratedTextColumn get status => _status ??= _constructStatus();
  GeneratedTextColumn _constructStatus() {
    return GeneratedTextColumn('status', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _typeMeta = const VerificationMeta('type');
  GeneratedTextColumn _type;
  GeneratedTextColumn get type => _type ??= _constructType();
  GeneratedTextColumn _constructType() {
    return GeneratedTextColumn('type', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  GeneratedTextColumn _description;
  GeneratedTextColumn get description =>
      _description ??= _constructDescription();
  GeneratedTextColumn _constructDescription() {
    return GeneratedTextColumn('description', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _totalByteLengthMeta =
      const VerificationMeta('totalByteLength');
  GeneratedIntColumn _totalByteLength;
  GeneratedIntColumn get totalByteLength =>
      _totalByteLength ??= _constructTotalByteLength();
  GeneratedIntColumn _constructTotalByteLength() {
    return GeneratedIntColumn('totalByteLength', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _languageCodeMeta =
      const VerificationMeta('languageCode');
  GeneratedTextColumn _languageCode;
  GeneratedTextColumn get languageCode =>
      _languageCode ??= _constructLanguageCode();
  GeneratedTextColumn _constructLanguageCode() {
    return GeneratedTextColumn('languageCode', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _lastSeenChapterMeta =
      const VerificationMeta('lastSeenChapter');
  GeneratedIntColumn _lastSeenChapter;
  GeneratedIntColumn get lastSeenChapter =>
      _lastSeenChapter ??= _constructLastSeenChapter();
  GeneratedIntColumn _constructLastSeenChapter() {
    return GeneratedIntColumn('lastSeenChapter', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _lastSeenPositionInChapterMeta =
      const VerificationMeta('lastSeenPositionInChapter');
  GeneratedIntColumn _lastSeenPositionInChapter;
  GeneratedIntColumn get lastSeenPositionInChapter =>
      _lastSeenPositionInChapter ??= _constructLastSeenPositionInChapter();
  GeneratedIntColumn _constructLastSeenPositionInChapter() {
    return GeneratedIntColumn('lastSeenPositionInChapter', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedIntColumn _updatedAt;
  GeneratedIntColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedIntColumn _constructUpdatedAt() {
    return GeneratedIntColumn('updatedAt', $tableName, true,
        $customConstraints: '');
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        uuid,
        title,
        authors,
        status,
        type,
        description,
        totalByteLength,
        languageCode,
        lastSeenChapter,
        lastSeenPositionInChapter,
        updatedAt
      ];
  @override
  DbBooks get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'dbBooks';
  @override
  final String actualTableName = 'dbBooks';
  @override
  VerificationContext validateIntegrity(DbBooksCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.uuid.present) {
      context.handle(
          _uuidMeta, uuid.isAcceptableValue(d.uuid.value, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (d.title.present) {
      context.handle(
          _titleMeta, title.isAcceptableValue(d.title.value, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (d.authors.present) {
      context.handle(_authorsMeta,
          authors.isAcceptableValue(d.authors.value, _authorsMeta));
    }
    if (d.status.present) {
      context.handle(
          _statusMeta, status.isAcceptableValue(d.status.value, _statusMeta));
    } else if (isInserting) {
      context.missing(_statusMeta);
    }
    if (d.type.present) {
      context.handle(
          _typeMeta, type.isAcceptableValue(d.type.value, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (d.description.present) {
      context.handle(_descriptionMeta,
          description.isAcceptableValue(d.description.value, _descriptionMeta));
    }
    if (d.totalByteLength.present) {
      context.handle(
          _totalByteLengthMeta,
          totalByteLength.isAcceptableValue(
              d.totalByteLength.value, _totalByteLengthMeta));
    }
    if (d.languageCode.present) {
      context.handle(
          _languageCodeMeta,
          languageCode.isAcceptableValue(
              d.languageCode.value, _languageCodeMeta));
    } else if (isInserting) {
      context.missing(_languageCodeMeta);
    }
    if (d.lastSeenChapter.present) {
      context.handle(
          _lastSeenChapterMeta,
          lastSeenChapter.isAcceptableValue(
              d.lastSeenChapter.value, _lastSeenChapterMeta));
    }
    if (d.lastSeenPositionInChapter.present) {
      context.handle(
          _lastSeenPositionInChapterMeta,
          lastSeenPositionInChapter.isAcceptableValue(
              d.lastSeenPositionInChapter.value,
              _lastSeenPositionInChapterMeta));
    }
    if (d.updatedAt.present) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableValue(d.updatedAt.value, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbBook map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DbBook.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DbBooksCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.uuid.present) {
      map['uuid'] = Variable<String, StringType>(d.uuid.value);
    }
    if (d.title.present) {
      map['title'] = Variable<String, StringType>(d.title.value);
    }
    if (d.authors.present) {
      map['authors'] = Variable<String, StringType>(d.authors.value);
    }
    if (d.status.present) {
      map['status'] = Variable<String, StringType>(d.status.value);
    }
    if (d.type.present) {
      map['type'] = Variable<String, StringType>(d.type.value);
    }
    if (d.description.present) {
      map['description'] = Variable<String, StringType>(d.description.value);
    }
    if (d.totalByteLength.present) {
      map['totalByteLength'] = Variable<int, IntType>(d.totalByteLength.value);
    }
    if (d.languageCode.present) {
      map['languageCode'] = Variable<String, StringType>(d.languageCode.value);
    }
    if (d.lastSeenChapter.present) {
      map['lastSeenChapter'] = Variable<int, IntType>(d.lastSeenChapter.value);
    }
    if (d.lastSeenPositionInChapter.present) {
      map['lastSeenPositionInChapter'] =
          Variable<int, IntType>(d.lastSeenPositionInChapter.value);
    }
    if (d.updatedAt.present) {
      map['updatedAt'] = Variable<int, IntType>(d.updatedAt.value);
    }
    return map;
  }

  @override
  DbBooks createAlias(String alias) {
    return DbBooks(_db, alias);
  }

  @override
  bool get dontWriteConstraints => true;
}

class DbChapter extends DataClass implements Insertable<DbChapter> {
  final int id;
  final String title;
  final int sortOrder;
  final int length;
  final int start;
  final int bookId;
  final String languageCode;
  final int updatedAt;
  DbChapter(
      {@required this.id,
      @required this.title,
      this.sortOrder,
      this.length,
      this.start,
      this.bookId,
      this.languageCode,
      this.updatedAt});
  factory DbChapter.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return DbChapter(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      title:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}title']),
      sortOrder:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}sortOrder']),
      length: intType.mapFromDatabaseResponse(data['${effectivePrefix}length']),
      start: intType.mapFromDatabaseResponse(data['${effectivePrefix}start']),
      bookId: intType.mapFromDatabaseResponse(data['${effectivePrefix}bookId']),
      languageCode: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}languageCode']),
      updatedAt:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}updatedAt']),
    );
  }
  factory DbChapter.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbChapter(
      id: serializer.fromJson<int>(json['id']),
      title: serializer.fromJson<String>(json['title']),
      sortOrder: serializer.fromJson<int>(json['sortOrder']),
      length: serializer.fromJson<int>(json['length']),
      start: serializer.fromJson<int>(json['start']),
      bookId: serializer.fromJson<int>(json['bookId']),
      languageCode: serializer.fromJson<String>(json['languageCode']),
      updatedAt: serializer.fromJson<int>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'title': serializer.toJson<String>(title),
      'sortOrder': serializer.toJson<int>(sortOrder),
      'length': serializer.toJson<int>(length),
      'start': serializer.toJson<int>(start),
      'bookId': serializer.toJson<int>(bookId),
      'languageCode': serializer.toJson<String>(languageCode),
      'updatedAt': serializer.toJson<int>(updatedAt),
    };
  }

  @override
  DbChaptersCompanion createCompanion(bool nullToAbsent) {
    return DbChaptersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      sortOrder: sortOrder == null && nullToAbsent
          ? const Value.absent()
          : Value(sortOrder),
      length:
          length == null && nullToAbsent ? const Value.absent() : Value(length),
      start:
          start == null && nullToAbsent ? const Value.absent() : Value(start),
      bookId:
          bookId == null && nullToAbsent ? const Value.absent() : Value(bookId),
      languageCode: languageCode == null && nullToAbsent
          ? const Value.absent()
          : Value(languageCode),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  DbChapter copyWith(
          {int id,
          String title,
          int sortOrder,
          int length,
          int start,
          int bookId,
          String languageCode,
          int updatedAt}) =>
      DbChapter(
        id: id ?? this.id,
        title: title ?? this.title,
        sortOrder: sortOrder ?? this.sortOrder,
        length: length ?? this.length,
        start: start ?? this.start,
        bookId: bookId ?? this.bookId,
        languageCode: languageCode ?? this.languageCode,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('DbChapter(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('sortOrder: $sortOrder, ')
          ..write('length: $length, ')
          ..write('start: $start, ')
          ..write('bookId: $bookId, ')
          ..write('languageCode: $languageCode, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          title.hashCode,
          $mrjc(
              sortOrder.hashCode,
              $mrjc(
                  length.hashCode,
                  $mrjc(
                      start.hashCode,
                      $mrjc(
                          bookId.hashCode,
                          $mrjc(
                              languageCode.hashCode, updatedAt.hashCode))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DbChapter &&
          other.id == this.id &&
          other.title == this.title &&
          other.sortOrder == this.sortOrder &&
          other.length == this.length &&
          other.start == this.start &&
          other.bookId == this.bookId &&
          other.languageCode == this.languageCode &&
          other.updatedAt == this.updatedAt);
}

class DbChaptersCompanion extends UpdateCompanion<DbChapter> {
  final Value<int> id;
  final Value<String> title;
  final Value<int> sortOrder;
  final Value<int> length;
  final Value<int> start;
  final Value<int> bookId;
  final Value<String> languageCode;
  final Value<int> updatedAt;
  const DbChaptersCompanion({
    this.id = const Value.absent(),
    this.title = const Value.absent(),
    this.sortOrder = const Value.absent(),
    this.length = const Value.absent(),
    this.start = const Value.absent(),
    this.bookId = const Value.absent(),
    this.languageCode = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  DbChaptersCompanion.insert({
    this.id = const Value.absent(),
    @required String title,
    this.sortOrder = const Value.absent(),
    this.length = const Value.absent(),
    this.start = const Value.absent(),
    this.bookId = const Value.absent(),
    this.languageCode = const Value.absent(),
    this.updatedAt = const Value.absent(),
  }) : title = Value(title);
  DbChaptersCompanion copyWith(
      {Value<int> id,
      Value<String> title,
      Value<int> sortOrder,
      Value<int> length,
      Value<int> start,
      Value<int> bookId,
      Value<String> languageCode,
      Value<int> updatedAt}) {
    return DbChaptersCompanion(
      id: id ?? this.id,
      title: title ?? this.title,
      sortOrder: sortOrder ?? this.sortOrder,
      length: length ?? this.length,
      start: start ?? this.start,
      bookId: bookId ?? this.bookId,
      languageCode: languageCode ?? this.languageCode,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }
}

class DbChapters extends Table with TableInfo<DbChapters, DbChapter> {
  final GeneratedDatabase _db;
  final String _alias;
  DbChapters(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _titleMeta = const VerificationMeta('title');
  GeneratedTextColumn _title;
  GeneratedTextColumn get title => _title ??= _constructTitle();
  GeneratedTextColumn _constructTitle() {
    return GeneratedTextColumn('title', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _sortOrderMeta = const VerificationMeta('sortOrder');
  GeneratedIntColumn _sortOrder;
  GeneratedIntColumn get sortOrder => _sortOrder ??= _constructSortOrder();
  GeneratedIntColumn _constructSortOrder() {
    return GeneratedIntColumn('sortOrder', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _lengthMeta = const VerificationMeta('length');
  GeneratedIntColumn _length;
  GeneratedIntColumn get length => _length ??= _constructLength();
  GeneratedIntColumn _constructLength() {
    return GeneratedIntColumn('length', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _startMeta = const VerificationMeta('start');
  GeneratedIntColumn _start;
  GeneratedIntColumn get start => _start ??= _constructStart();
  GeneratedIntColumn _constructStart() {
    return GeneratedIntColumn('start', $tableName, true,
        $customConstraints: 'DEFAULT 0',
        defaultValue: const CustomExpression<int, IntType>('0'));
  }

  final VerificationMeta _bookIdMeta = const VerificationMeta('bookId');
  GeneratedIntColumn _bookId;
  GeneratedIntColumn get bookId => _bookId ??= _constructBookId();
  GeneratedIntColumn _constructBookId() {
    return GeneratedIntColumn('bookId', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _languageCodeMeta =
      const VerificationMeta('languageCode');
  GeneratedTextColumn _languageCode;
  GeneratedTextColumn get languageCode =>
      _languageCode ??= _constructLanguageCode();
  GeneratedTextColumn _constructLanguageCode() {
    return GeneratedTextColumn('languageCode', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedIntColumn _updatedAt;
  GeneratedIntColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedIntColumn _constructUpdatedAt() {
    return GeneratedIntColumn('updatedAt', $tableName, true,
        $customConstraints: '');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, title, sortOrder, length, start, bookId, languageCode, updatedAt];
  @override
  DbChapters get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'dbChapters';
  @override
  final String actualTableName = 'dbChapters';
  @override
  VerificationContext validateIntegrity(DbChaptersCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.title.present) {
      context.handle(
          _titleMeta, title.isAcceptableValue(d.title.value, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (d.sortOrder.present) {
      context.handle(_sortOrderMeta,
          sortOrder.isAcceptableValue(d.sortOrder.value, _sortOrderMeta));
    }
    if (d.length.present) {
      context.handle(
          _lengthMeta, length.isAcceptableValue(d.length.value, _lengthMeta));
    }
    if (d.start.present) {
      context.handle(
          _startMeta, start.isAcceptableValue(d.start.value, _startMeta));
    }
    if (d.bookId.present) {
      context.handle(
          _bookIdMeta, bookId.isAcceptableValue(d.bookId.value, _bookIdMeta));
    }
    if (d.languageCode.present) {
      context.handle(
          _languageCodeMeta,
          languageCode.isAcceptableValue(
              d.languageCode.value, _languageCodeMeta));
    }
    if (d.updatedAt.present) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableValue(d.updatedAt.value, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbChapter map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DbChapter.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DbChaptersCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.title.present) {
      map['title'] = Variable<String, StringType>(d.title.value);
    }
    if (d.sortOrder.present) {
      map['sortOrder'] = Variable<int, IntType>(d.sortOrder.value);
    }
    if (d.length.present) {
      map['length'] = Variable<int, IntType>(d.length.value);
    }
    if (d.start.present) {
      map['start'] = Variable<int, IntType>(d.start.value);
    }
    if (d.bookId.present) {
      map['bookId'] = Variable<int, IntType>(d.bookId.value);
    }
    if (d.languageCode.present) {
      map['languageCode'] = Variable<String, StringType>(d.languageCode.value);
    }
    if (d.updatedAt.present) {
      map['updatedAt'] = Variable<int, IntType>(d.updatedAt.value);
    }
    return map;
  }

  @override
  DbChapters createAlias(String alias) {
    return DbChapters(_db, alias);
  }

  @override
  List<String> get customConstraints => const [
        'CONSTRAINT fk_dbbooks\n    FOREIGN KEY (bookId)\n    REFERENCES dbBooks(id)\n    ON DELETE CASCADE'
      ];
  @override
  bool get dontWriteConstraints => true;
}

class DbExampleSentence extends DataClass
    implements Insertable<DbExampleSentence> {
  final int id;
  final String sentence;
  final String translation;
  final int tokenId;
  final String words;
  DbExampleSentence(
      {@required this.id,
      @required this.sentence,
      this.translation,
      this.tokenId,
      @required this.words});
  factory DbExampleSentence.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return DbExampleSentence(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      sentence: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}sentence']),
      translation: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}translation']),
      tokenId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}tokenId']),
      words:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}words']),
    );
  }
  factory DbExampleSentence.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DbExampleSentence(
      id: serializer.fromJson<int>(json['id']),
      sentence: serializer.fromJson<String>(json['sentence']),
      translation: serializer.fromJson<String>(json['translation']),
      tokenId: serializer.fromJson<int>(json['tokenId']),
      words: serializer.fromJson<String>(json['words']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'sentence': serializer.toJson<String>(sentence),
      'translation': serializer.toJson<String>(translation),
      'tokenId': serializer.toJson<int>(tokenId),
      'words': serializer.toJson<String>(words),
    };
  }

  @override
  DbExampleSentencesCompanion createCompanion(bool nullToAbsent) {
    return DbExampleSentencesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      sentence: sentence == null && nullToAbsent
          ? const Value.absent()
          : Value(sentence),
      translation: translation == null && nullToAbsent
          ? const Value.absent()
          : Value(translation),
      tokenId: tokenId == null && nullToAbsent
          ? const Value.absent()
          : Value(tokenId),
      words:
          words == null && nullToAbsent ? const Value.absent() : Value(words),
    );
  }

  DbExampleSentence copyWith(
          {int id,
          String sentence,
          String translation,
          int tokenId,
          String words}) =>
      DbExampleSentence(
        id: id ?? this.id,
        sentence: sentence ?? this.sentence,
        translation: translation ?? this.translation,
        tokenId: tokenId ?? this.tokenId,
        words: words ?? this.words,
      );
  @override
  String toString() {
    return (StringBuffer('DbExampleSentence(')
          ..write('id: $id, ')
          ..write('sentence: $sentence, ')
          ..write('translation: $translation, ')
          ..write('tokenId: $tokenId, ')
          ..write('words: $words')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          sentence.hashCode,
          $mrjc(
              translation.hashCode, $mrjc(tokenId.hashCode, words.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DbExampleSentence &&
          other.id == this.id &&
          other.sentence == this.sentence &&
          other.translation == this.translation &&
          other.tokenId == this.tokenId &&
          other.words == this.words);
}

class DbExampleSentencesCompanion extends UpdateCompanion<DbExampleSentence> {
  final Value<int> id;
  final Value<String> sentence;
  final Value<String> translation;
  final Value<int> tokenId;
  final Value<String> words;
  const DbExampleSentencesCompanion({
    this.id = const Value.absent(),
    this.sentence = const Value.absent(),
    this.translation = const Value.absent(),
    this.tokenId = const Value.absent(),
    this.words = const Value.absent(),
  });
  DbExampleSentencesCompanion.insert({
    this.id = const Value.absent(),
    @required String sentence,
    this.translation = const Value.absent(),
    this.tokenId = const Value.absent(),
    @required String words,
  })  : sentence = Value(sentence),
        words = Value(words);
  DbExampleSentencesCompanion copyWith(
      {Value<int> id,
      Value<String> sentence,
      Value<String> translation,
      Value<int> tokenId,
      Value<String> words}) {
    return DbExampleSentencesCompanion(
      id: id ?? this.id,
      sentence: sentence ?? this.sentence,
      translation: translation ?? this.translation,
      tokenId: tokenId ?? this.tokenId,
      words: words ?? this.words,
    );
  }
}

class DbExampleSentences extends Table
    with TableInfo<DbExampleSentences, DbExampleSentence> {
  final GeneratedDatabase _db;
  final String _alias;
  DbExampleSentences(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        declaredAsPrimaryKey: true,
        hasAutoIncrement: true,
        $customConstraints: 'NOT NULL PRIMARY KEY AUTOINCREMENT');
  }

  final VerificationMeta _sentenceMeta = const VerificationMeta('sentence');
  GeneratedTextColumn _sentence;
  GeneratedTextColumn get sentence => _sentence ??= _constructSentence();
  GeneratedTextColumn _constructSentence() {
    return GeneratedTextColumn('sentence', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  final VerificationMeta _translationMeta =
      const VerificationMeta('translation');
  GeneratedTextColumn _translation;
  GeneratedTextColumn get translation =>
      _translation ??= _constructTranslation();
  GeneratedTextColumn _constructTranslation() {
    return GeneratedTextColumn('translation', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _tokenIdMeta = const VerificationMeta('tokenId');
  GeneratedIntColumn _tokenId;
  GeneratedIntColumn get tokenId => _tokenId ??= _constructTokenId();
  GeneratedIntColumn _constructTokenId() {
    return GeneratedIntColumn('tokenId', $tableName, true,
        $customConstraints: '');
  }

  final VerificationMeta _wordsMeta = const VerificationMeta('words');
  GeneratedTextColumn _words;
  GeneratedTextColumn get words => _words ??= _constructWords();
  GeneratedTextColumn _constructWords() {
    return GeneratedTextColumn('words', $tableName, false,
        $customConstraints: 'NOT NULL');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, sentence, translation, tokenId, words];
  @override
  DbExampleSentences get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'dbExampleSentences';
  @override
  final String actualTableName = 'dbExampleSentences';
  @override
  VerificationContext validateIntegrity(DbExampleSentencesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.sentence.present) {
      context.handle(_sentenceMeta,
          sentence.isAcceptableValue(d.sentence.value, _sentenceMeta));
    } else if (isInserting) {
      context.missing(_sentenceMeta);
    }
    if (d.translation.present) {
      context.handle(_translationMeta,
          translation.isAcceptableValue(d.translation.value, _translationMeta));
    }
    if (d.tokenId.present) {
      context.handle(_tokenIdMeta,
          tokenId.isAcceptableValue(d.tokenId.value, _tokenIdMeta));
    }
    if (d.words.present) {
      context.handle(
          _wordsMeta, words.isAcceptableValue(d.words.value, _wordsMeta));
    } else if (isInserting) {
      context.missing(_wordsMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DbExampleSentence map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DbExampleSentence.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DbExampleSentencesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.sentence.present) {
      map['sentence'] = Variable<String, StringType>(d.sentence.value);
    }
    if (d.translation.present) {
      map['translation'] = Variable<String, StringType>(d.translation.value);
    }
    if (d.tokenId.present) {
      map['tokenId'] = Variable<int, IntType>(d.tokenId.value);
    }
    if (d.words.present) {
      map['words'] = Variable<String, StringType>(d.words.value);
    }
    return map;
  }

  @override
  DbExampleSentences createAlias(String alias) {
    return DbExampleSentences(_db, alias);
  }

  @override
  List<String> get customConstraints => const [
        'CONSTRAINT fk_dbtokens\n    FOREIGN KEY (tokenId)\n    REFERENCES dbTokens(id)\n    ON DELETE CASCADE'
      ];
  @override
  bool get dontWriteConstraints => true;
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  DbTokens _dbTokens;
  DbTokens get dbTokens => _dbTokens ??= DbTokens(this);
  Index _idxDbTokensNameLang;
  Index get idxDbTokensNameLang => _idxDbTokensNameLang ??= Index(
      'idx_dbTokens_name_lang',
      'CREATE UNIQUE INDEX idx_dbTokens_name_lang ON dbTokens (name, languageCode);');
  DbLanguages _dbLanguages;
  DbLanguages get dbLanguages => _dbLanguages ??= DbLanguages(this);
  Index _idxDbLanguagesCode3;
  Index get idxDbLanguagesCode3 => _idxDbLanguagesCode3 ??= Index(
      'idx_dbLanguages_code3',
      'CREATE UNIQUE INDEX idx_dbLanguages_code3 ON dbLanguages (code3);');
  DbBooks _dbBooks;
  DbBooks get dbBooks => _dbBooks ??= DbBooks(this);
  DbChapters _dbChapters;
  DbChapters get dbChapters => _dbChapters ??= DbChapters(this);
  DbExampleSentences _dbExampleSentences;
  DbExampleSentences get dbExampleSentences =>
      _dbExampleSentences ??= DbExampleSentences(this);
  TokensDao _tokensDao;
  TokensDao get tokensDao => _tokensDao ??= TokensDao(this as MyDatabase);
  LanguagesDao _languagesDao;
  LanguagesDao get languagesDao =>
      _languagesDao ??= LanguagesDao(this as MyDatabase);
  BooksDao _booksDao;
  BooksDao get booksDao => _booksDao ??= BooksDao(this as MyDatabase);
  DbLanguage _rowToDbLanguage(QueryRow row) {
    return DbLanguage(
      id: row.readInt('id'),
      name: row.readString('name'),
      code2: row.readString('code2'),
      code3: row.readString('code3'),
      ttsEngine: row.readString('ttsEngine'),
      ttsVoice: row.readString('ttsVoice'),
    );
  }

  Selectable<DbLanguage> findLanguageByCode(String code) {
    return customSelectQuery(
        'SELECT * FROM dbLanguages WHERE code2 = :code OR code3 = :code',
        variables: [Variable.withString(code)],
        readsFrom: {dbLanguages}).map(_rowToDbLanguage);
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        dbTokens,
        idxDbTokensNameLang,
        dbLanguages,
        idxDbLanguagesCode3,
        dbBooks,
        dbChapters,
        dbExampleSentences
      ];
}
