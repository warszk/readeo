import 'package:moor/moor.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/models/book.model.dart';

part 'books.dao.g.dart';

@UseDao(tables: [DbBooks])
class BooksDao extends DatabaseAccessor<MyDatabase> with _$BooksDaoMixin {
  BooksDao(MyDatabase db) : super(db);

  Stream<List<DbBook>> watchBookStream() {
    return select(db.dbBooks).watch();
  }

  Future<void> addBook(BookModel bm) async {
    await into(db.dbBooks).insert(bm.toDbBook());
  }

  Future<void> updateBook(BookModel bm) async {
    await update(db.dbBooks).replace(bm.toDbBook());
  }

  Future<void> removeBook(BookModel bm) async {
    return await (delete(db.dbBooks)..where((t) => t.id.equals(bm.id))).go();
  }

  Future<BookModel> getBookByUuid(String uuid) async {
    var b = await (select(db.dbBooks)..where((b) => b.uuid.equals(uuid)))
        .getSingle();
    if (b != null) {
      return BookModel.fromBook(b);
    }

    return null;
  }
}
