import 'package:moor/moor.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/models/language.model.dart';

part 'languages.dao.g.dart';

@UseDao(tables: [DbLanguages])
class LanguagesDao extends DatabaseAccessor<MyDatabase> with _$LanguagesDaoMixin {
  LanguagesDao(MyDatabase db) : super(db);

  Future<List<DbLanguage>> getLanguages() async {
    return select(db.dbLanguages).get();
  }

  Future<LanguageModel> getLanguage(String code) async {
    var langs = await db.findLanguageByCode(code).get();

    if(langs.isNotEmpty) {
      return LanguageModel.fromDB(langs.first);
    }
    return null;
  }

  Stream<List<DbLanguage>> watchLanguagesStream() {
    return select(db.dbLanguages).watch();
  }
}
