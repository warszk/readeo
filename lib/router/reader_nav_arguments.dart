import 'package:readeo/models/book.model.dart';

class ReaderNavArguments {
  final BookModel book;

  ReaderNavArguments(this.book);
}
