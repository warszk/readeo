import 'package:readeo/models/book.model.dart';

class BookDescriptionNavArguments {
  BookModel book;

  BookDescriptionNavArguments(this.book);
}
