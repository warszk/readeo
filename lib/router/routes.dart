const String HomeRoute = "/";
const String BookDescriptionRoute = "/book";
const String OpdsRoute = "/opds";
const String OpdsBookDescriptionRoute = "/opdsbook";
const String LanguagesRoute = "/languages";
const String TokensRoute = "/tokens";
const String ReaderRoute = "/reader";
