import 'package:readeo/opds/parser/calibre_parser.dart';

class OpdsBookDescriptionNavArguments {
  BookEntry book;

  OpdsBookDescriptionNavArguments(this.book);
}
