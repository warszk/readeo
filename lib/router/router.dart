import 'package:flutter/material.dart';
import 'package:readeo/languages/languagelist.view.dart';
import 'package:readeo/opds/opds.widget.dart';
import 'package:readeo/reader/ui/reader.view.dart';
import 'package:readeo/router/reader_nav_arguments.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/router/undefined.view.dart';
import 'package:readeo/shelf/bookdescription.view.dart';
import 'package:readeo/router/bookdescription_nav_arguments.dart';
import 'package:readeo/opds/opdsbookdescription.view.dart';
import 'package:readeo/router/opdsbookdescription_nav_arguments.dart';
import 'package:readeo/shelf/shelf.view.dart';
import 'package:readeo/tokens/tokenlist.view.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch(settings.name) {
    case HomeRoute:
      return MaterialPageRoute(builder: (context) => ShelfView());
    case BookDescriptionRoute:
      final BookDescriptionNavArguments args = settings.arguments;
      return MaterialPageRoute(builder: (context) => BookDescriptionView(args));
    case OpdsBookDescriptionRoute:
      final OpdsBookDescriptionNavArguments args = settings.arguments;
      return MaterialPageRoute(builder: (context) => OpdsBookDescriptionView(args));
    case LanguagesRoute:
      return MaterialPageRoute(builder: (context) => LanguageListView());
    case TokensRoute:
      return MaterialPageRoute(builder: (context) => TokenListView());
    case ReaderRoute:
      final ReaderNavArguments args = settings.arguments;
      return MaterialPageRoute(builder: (context) => ReaderView(args));
    case OpdsRoute:
      return MaterialPageRoute(builder: (context) => OpdsView());
    default:
      return MaterialPageRoute(builder: (context) => UndefinedView(name: settings.name,));
  }
}
