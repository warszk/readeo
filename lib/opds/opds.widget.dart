import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/opds/opds_provider.dart';
import 'package:readeo/router/opdsbookdescription_nav_arguments.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/opds/parser/calibre_parser.dart';

class OpdsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<OpdsProvider>(
      create: (BuildContext context) => OpdsProvider(),
      child: _ScaffoldView(),
      dispose: (BuildContext context, OpdsProvider provider) =>
          provider.dispose(),
    );
  }
}

class _ScaffoldView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final opdsProvider = Provider.of<OpdsProvider>(context);
    opdsProvider.discoverCalibreOPDS();

    return Scaffold(
      backgroundColor: ReaderColors.bg,
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: AppColors.primaryColor,
        title: Text("Opds"),
      ),
      body: _OpdsWidget(),
    );
  }
}

class _OpdsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final opdsProvider = Provider.of<OpdsProvider>(context);
    return StreamBuilder<List<Entry>>(
      initialData: opdsProvider.currentBookEntries,
      stream: opdsProvider.calibreCategories,
      builder: (context, AsyncSnapshot<List<Entry>> snapshot) {
        if (snapshot.hasData) {
          if (opdsProvider.shouldDisplayBookShelf) {
            return _CalibreShelfGrid(snapshot.data);
          } else {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, el) =>
                  _CalibreCategoryElementWidget(snapshot.data[el]),
            );
          }
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        } else {
          return Center(
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    "Configure a local Calibre application on your PC",
                    style: TextStyle(
                        color: AppColors.primaryTextColor, fontSize: 21),
                    textAlign: TextAlign.center,
                  )));
        }
      },
    );
  }
}

class _CalibreShelfGrid extends StatelessWidget {
  final List<Entry> books;

  _CalibreShelfGrid(this.books);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      primary: false,
      slivers: <Widget>[
        SliverPadding(
          padding: EdgeInsets.all(16),
          sliver: SliverGrid.count(
            childAspectRatio: 2 / 3,
            crossAxisCount: 3,
            mainAxisSpacing: 20.0,
            crossAxisSpacing: 20.0,
            children: books.map((book) => _CalibreBookTile(book)).toList(),
          ),
        )
      ],
    );
  }
}

class _CalibreBookTile extends StatelessWidget {
  final BookEntry bookEntry;
  _CalibreBookTile(this.bookEntry);
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 15,
      shadowColor: Colors.yellow.shade900,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(OpdsBookDescriptionRoute,
              arguments: OpdsBookDescriptionNavArguments(bookEntry));
        },
        onLongPress: () {
          // Flushbar(
          //   message: bookEntry.title,
          //   icon: Icon(Icons.import_contacts),
          // ).show(context);
        },
        child: Hero(
            tag: "opds-${bookEntry.id}",
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl: bookEntry.coverUrl,
              placeholder: (context, url) => Placeholder(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
      ),
    );
  }
}

class _CalibreCategoryElementWidget extends StatelessWidget {
  final Entry entry;

  _CalibreCategoryElementWidget(this.entry);

  @override
  Widget build(BuildContext context) {
    final opdsProvider = Provider.of<OpdsProvider>(context);

    return Padding(
        padding: EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
        child: InkWell(
            onTap: () {
              opdsProvider.connectToLocalCalibre(entry.entryUrl);
            },
            child: Card(
              color: AppColors.secondaryColor,
              elevation: 8,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  children: <Widget>[
                    Text(
                      entry.title,
                      style: TextStyle(
                          fontSize: 18, color: AppColors.primaryTextColor),
                    )
                  ],
                ),
              ),
            )));
  }
}
