import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';
import 'package:readeo/opds/parser/calibre_parser.dart';
import 'package:readeo/opds/download_provider.dart';

class OpdsBookDownloadWidget extends StatelessWidget {
  final BookEntry bookEntry;
  OpdsBookDownloadWidget(this.bookEntry);

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<MyDatabase>(context);
    return Provider<DownloadProvider>(
      create: (BuildContext context) => DownloadProvider(db),
      child: _ScaffoldView(bookEntry),
    );
  }
}

class _ScaffoldView extends StatelessWidget {
  final BookEntry bookEntry;
  _ScaffoldView(this.bookEntry);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<DbLanguage>>(
      future: Provider.of<MyDatabase>(context).languagesDao.getLanguages(),
      builder: (context, AsyncSnapshot<List<DbLanguage>> snapshot) {
        if (snapshot.hasData) {
          return _LanguageChooser(snapshot.data, bookEntry);
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class _LanguageChooser extends StatefulWidget {
  final List<DbLanguage> languages;
  final BookEntry bookEntry;

  _LanguageChooser(this.languages, this.bookEntry);
  @override
  _LanguageChooserState createState() => _LanguageChooserState();
}

class _LanguageChooserState extends State<_LanguageChooser> {
  int groupValue = 0;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DownloadProvider>(context);

    return CustomDialogWidget(
        icon: Icon(Icons.language),
        title: "Choose language",
        buttons: ButtonBar(
          children: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              color: Colors.blue,
              child: Text("Select"),
              onPressed: () {
                provider.downloadBook(
                    widget.bookEntry, widget.languages[groupValue].code3,
                    isLocal: false);
                    Navigator.of(context).pop();
              },
            ),
          ],
        ),
        body: Container(
            child: ListView.builder(
          shrinkWrap: true,
          itemCount: widget.languages.length,
          itemBuilder: (context, index) {
            return Row(children: [
              Radio(
                value: index,
                groupValue: groupValue,
                onChanged: (i) {
                  setState(() {
                    groupValue = index;
                  });
                },
              ),
              Text(widget.languages[index].name)
            ]);
          },
        )));
  }
}
