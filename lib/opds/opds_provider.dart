import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:mdns_plugin/mdns_plugin.dart';
import 'package:readeo/models/book.model.dart';

import 'package:readeo/opds/parser/calibre_parser.dart';

class OpdsProvider implements MDNSPluginDelegate {
  MDNSPlugin mdns;
  String opdsHost = "http://192.168.0.206:8080";

  bool shouldDisplayBookShelf = false;
  bool shouldDisplayOpdsBookShelf = false;

  List<Entry> currentBookEntries = List();
  List<Entry> currentOpdsEntries = List();
  List<BookModel> currentBookModels = List();

  //opds stream
  final _calibreStream = StreamController<List<Entry>>.broadcast();
  Stream get calibreCategories => _calibreStream.stream;

  Future<void> connectToLocalCalibre(address, {bool addMore = false}) async {
    if (!addMore) currentBookEntries.clear();

    try {
      var response = await http.get("$opdsHost$address");
      if (response.statusCode == 200) {
        CalibreParser parser = CalibreParser(opdsHost, address)
          ..parse(response.body);

        if (parser.bookEntries.isNotEmpty) {
          shouldDisplayBookShelf = true;
          currentBookEntries.addAll(parser.bookEntries);
        } else {
          shouldDisplayBookShelf = false;
          currentBookEntries.addAll(parser.entries);
        }
        _calibreStream.sink.add(currentBookEntries);
      }
    } catch (e) {
      _calibreStream.addError("No connection");
    }
  }

  getNextBooks(url) {
    connectToLocalCalibre(url, addMore: true);
  }

  Future<void> discoverCalibreOPDS() async {
    mdns = MDNSPlugin(this);
    await mdns.startDiscovery("_calibre._tcp", enableUpdating: true);
  }

  void onDiscoveryStarted() {
    print("started");
  }

  void onDiscoveryStopped() {
    print("stopped");
  }

  bool onServiceFound(MDNSService service) {
    // Always returns true which begins service resolution
    print("found");
    return true;
  }

  void onServiceResolved(MDNSService service) async {
    print("resolved");
    // service discovery doesn't work currently - why?
    var path = service.txt.containsKey("path")
        ? Utf8Codec().decode(service.txt["path"])
        : "opds";
    //todo: try http and https...
    //todo: add auth
    opdsHost = "http://${service.hostName}:${service.port}";
    connectToLocalCalibre(path);
  }

  void onServiceUpdated(MDNSService service) {}
  void onServiceRemoved(MDNSService service) {}

  dispose() {
    mdns.stopDiscovery();
    _calibreStream.close();
  }
}
