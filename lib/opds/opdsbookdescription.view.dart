import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/opds/description_provider.dart';
import 'package:readeo/router/reader_nav_arguments.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/opds/parser/calibre_parser.dart';
import 'package:readeo/opds/opds_book_download.widget.dart';
import 'package:readeo/router/opdsbookdescription_nav_arguments.dart';

class OpdsBookDescriptionView extends StatelessWidget {
  final BookEntry book;

  OpdsBookDescriptionView(OpdsBookDescriptionNavArguments args)
      : book = args.book;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DescriptionProvider>(
        create: (BuildContext context) {
          return DescriptionProvider(book);
        },
        child: _ScaffoldView(book));
  }
}

class _ScaffoldView extends StatelessWidget {
  final BookEntry book;

  _ScaffoldView(this.book);

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<DescriptionProvider>(context);
    var db = Provider.of<MyDatabase>(context);
    provider.getBook(db);

    return Scaffold(
      appBar: AppBar(
        title: Text("Description"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_Header(), Expanded(child: _DescriptionBox())],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<DescriptionProvider>(context);
    return Container(
      padding: EdgeInsets.all(20),
      color: AppColors.primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Hero(
            tag: "opds-${provider.bookEntry.id}",
            child: CachedNetworkImage(
              width: 150,
              fit: BoxFit.cover,
              imageUrl: provider.bookEntry.coverUrl,
              placeholder: (context, url) => Placeholder(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 20),
              Text(
                provider.bookEntry.title,
                style: TextStyle(
                    color: AppColors.primaryTextColor,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              Text(provider.bookEntry.language ?? "",
                  style: TextStyle(color: AppColors.secondaryTextColor)),
              SizedBox(height: 40),
              Align(
                  alignment: Alignment.bottomRight,
                  child: ButtonBar(
                    children: <Widget>[
                      _ButtonBox(),
                    ],
                  ))
            ],
          ))
        ],
      ),
    );
  }
}

class _ButtonBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<DescriptionProvider>(context);
    if (provider.bookEntry.entryUrl == null ||
        provider.bookEntry.entryUrl.isEmpty) {
      return Center(child: Text("Convert this file to epub", style: TextStyle(color: AppColors.secondaryTextColor),));
    }

    if (provider.bookModel != null) {
      return RaisedButton(
        child: Text("Read"),
        color: Colors.green,
        onPressed: () {
          Navigator.of(context).pushNamed(ReaderRoute,
              arguments: ReaderNavArguments(provider.bookModel));
        },
      );
    } else {
      return RaisedButton(
        child: Text("Download"),
        color: Colors.green,
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) => OpdsBookDownloadWidget(provider.bookEntry));
        },
      );
    }
  }
}

class _DescriptionBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<DescriptionProvider>(context);
    return Container(
        constraints: BoxConstraints.expand(),
        color: AppColors.secondaryColor,
        child: Padding(
            padding: EdgeInsets.all(20),
            child: SingleChildScrollView(
                child: Text(
              provider.bookEntry.desc ?? "No description",
              style: TextStyle(
                  color: AppColors.primaryTextColor, fontSize: 18, height: 1.5),
              textAlign: TextAlign.justify,
            ))));
  }
}
