import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:html/parser.dart' show parse;
import 'package:dio/dio.dart';
import 'package:epub/epub.dart';
import 'package:path_provider/path_provider.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/models/book.model.dart';
import 'package:readeo/models/toc.model.dart';

import 'package:readeo/opds/parser/calibre_parser.dart';

class DownloadProvider {
  final MyDatabase db;
  int progress = 0;
  String status;

  bool get isDownloading => progress > 0;
  bool get isDone => progress == 100 && status == null;
  bool get isNotStarted => progress == 0 && status == null;

  DownloadProvider(this.db);

  Future<BookModel> downloadBook(BookEntry entry, String langCode,
      {bool isLocal = true}) async {
    // todo
    // do not download if the book is already stored in the db
    print("1 $status");
    await requestReadWritePermission();
    var path = (await getApplicationDocumentsDirectory()).path;

    status = "Downloading ebook";
    print("2 $status");

    await File(path + "/${entry.id}/cover").create(recursive: true);

    var fileName = path + "/${entry.id}.epub";
    await Dio().download(
      "${entry.entryUrl}",
      fileName,
      onReceiveProgress: (receivedBytes, totalBytes) {
        progress = (receivedBytes / totalBytes * 100).round();
      },
    );

    print("3 $status");
    status = "Downloading cover";
    print(4);

    // download cover
    var coverFileName = path + "/${entry.id}/cover";
    await Dio().download(
      "${entry.coverUrl}",
      coverFileName,
      onReceiveProgress: (receivedBytes, totalBytes) {
        progress = (receivedBytes / totalBytes * 100).round();
      },
    );

    await processRawBookFile("$path/${entry.id}", "$path/${entry.id}.epub");

    // write to db
    var book = BookModel(
        uuid: entry.id,
        title: entry.title,
        description: entry.desc,
        langCode: langCode,
        status: "DONE",
        type: "epub");
    db.booksDao.addBook(book);
    print("dpo");
    return book;
  }

  Future<bool> requestReadWritePermission() async {
    if (await Permission.storage.request().isGranted) {
      return true;
    }

    return false;
  }

  Future<void> processRawBookFile(String rootpath, String rawFilePath) async {
    status = "Processing";
    EpubBook epub =
        await EpubReader.readBook(await File(rawFilePath).readAsBytes());

    List<Chapter> _pubChapters = List();
    var num = epub.Chapters.length + 1;

    var i = 0;
    for (var chapter in epub.Chapters) {
      var txt = parse(chapter.HtmlContent).body.text.trim();
      if (txt != null && txt.isNotEmpty) {
        _pubChapters
            .add(Chapter(name: chapter.Title, href: chapter.ContentFileName));
        var file = File('$rootpath/${chapter.ContentFileName}');
        await file.create(recursive: true);
        await file.writeAsString(chapter.HtmlContent);
      }
      for (var subchapter in chapter.SubChapters) {
        var txt = parse(chapter.HtmlContent).body.text.trim();
        if (txt != null && txt.isNotEmpty) {
          _pubChapters.add(Chapter(
              name: subchapter.Title, href: subchapter.ContentFileName));
          var file = File('$rootpath/${subchapter.ContentFileName}');
          await file.create(recursive: true);
          await file.writeAsString(subchapter.HtmlContent);
        }
      }
      progress = (i++ / num * 100).round();
    }

    status = "Finalizing";

    var file = File('$rootpath/toc.json');
    await file.create(recursive: true);
    await file.writeAsString(jsonEncode(_pubChapters));
    progress = 100;
    status = null;
  }

  dispose() {}
}
