import 'package:flutter/widgets.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/models/book.model.dart';
import 'package:readeo/opds/parser/calibre_parser.dart';

class DescriptionProvider extends ChangeNotifier {
  final BookEntry bookEntry;
  BookModel bookModel;

  DescriptionProvider(this.bookEntry);

  getBook(MyDatabase db) async {
    bookModel = await db.booksDao.getBookByUuid(bookEntry.id);
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
