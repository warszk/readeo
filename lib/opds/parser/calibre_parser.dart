import 'dart:convert';

import 'package:xml/xml.dart' as xml;

class Entry {
  String hostUrl;
  String title;
  String id;
  DateTime updatedAt;
  String content;
  String entryUrl;

  bool get isBook => false;

  Entry({this.title});

  @override
  String toString() {
    return "$title - $entryUrl";
  }
}

class BookEntry extends Entry {
  String author;
  DateTime publishedAt;
  DateTime date;
  String pubUrl;
  int length;
  String coverUrl;
  String summary;
  String language;

  String get desc => summary != null ? summary : content;
  bool get isBook => true;
}

class CalibreParser {
  final String hostUrl;
  final String hostname;
  String title;
  String upUrl;
  String nextUrl;
  List<Entry> entries = List();
  List<BookEntry> bookEntries = List();

  CalibreParser(this.hostname, String path) : this.hostUrl = "$hostname$path";

  bool get hasNext => nextUrl != null && nextUrl.isNotEmpty;

  parse(String content, {bool clear = true}) {
    if (clear) {
      entries.clear();
      bookEntries.clear();
    }
    var doc = xml.parse(content);
    var feed = doc.findElements("feed").first;

    feed.findAllElements("entry").forEach((entry) {
      _parseEntry(entry);
    });

    feed.findElements("link").forEach((link) {
      if (link.getAttribute('rel') == "next") {
        nextUrl = link.getAttribute('href');
      }

      if (link.getAttribute('rel') == "up") {
        upUrl = link.getAttribute('href');
      }
    });

    title = feed.findElements('title').toList().first.text;
  }

  _parseEntry(xml.XmlElement entry) {
    var firstChild = entry.firstChild;
    xml.XmlNode nextEl = firstChild.nextSibling;
    bool isBook = _isBookEntry(entry);
    Entry _entry = isBook ? BookEntry() : Entry();

    _entry.hostUrl = hostUrl;

    while (nextEl != null) {
      if (nextEl.nodeType == xml.XmlNodeType.ELEMENT) {
        switch ((nextEl as xml.XmlElement).name.qualified.trim()) {
          case "title":
            _entry.title = nextEl.text;
            break;
          case "id":
            Codec<String, String> stringToBase64 = utf8.fuse(base64);
            _entry.id = stringToBase64.encode(nextEl.text.split(":").last);
            break;
          case "link":
            var rel = (nextEl as xml.XmlElement).getAttribute("rel");
            var href = (nextEl as xml.XmlElement).getAttribute("href");
            var type = (nextEl as xml.XmlElement).getAttribute("type");
            if (!isBook) {
              _entry.entryUrl = href;
            } else if (rel != null) {
              if (rel == "http://opds-spec.org/cover" ||
                  rel == "http://opds-spec.org/image") {
                (_entry as BookEntry).coverUrl = "$hostname$href";
              } else if ((rel == "http://opds-spec.org/acquisition" ||
                      rel == "http://opds-spec.org/acquisition/sample") &&
                  type == "application/epub+zip") {
                (_entry as BookEntry).entryUrl = "$hostname$href";
              }
            }
            break;
          case "content":
            _entry.content = nextEl.text;
            break;
          case "summary":
            (_entry as BookEntry).summary = nextEl.text;
            break;
          case "dcterms:language":
            (_entry as BookEntry).language = nextEl.text;
            break;
        }
      }

      nextEl = nextEl.nextSibling;
    }

    if (_entry is BookEntry) {
      bookEntries.add(_entry);
    } else {
      entries.add(_entry);
    }
  }

  bool _isBookEntry(xml.XmlElement element) {
    return element.findElements('link').any((el) => [
          "http://opds-spec.org/acquisition/sample",
          "http://opds-spec.org/acquisition",
          "http://opds-spec.org/acquisition/buy"
        ].contains(el.getAttribute('rel')));
  }

  @override
  String toString() {
    return "$title - $nextUrl - $upUrl";
  }
}
