
import 'package:flutter/material.dart';
import 'package:readeo/shared/colors.dart';

class CustomDialogWidget extends StatelessWidget {
  final Widget body;
  final Widget buttons;
  final String title;
  final Icon icon;
  final Color backgroundColor;

  CustomDialogWidget({this.title, this.body, this.buttons, this.icon, this.backgroundColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0)),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 60, bottom: 20, left: 20, right: 20),
              margin: EdgeInsets.only(top: 40),
              decoration: BoxDecoration(
                color: backgroundColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: DialogBoxColors.shadowColor,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0)
                  )
                ]
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      title,
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700)
                    ),
                    SizedBox(height: 16),
                    body,
                    SizedBox(height: 16),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: buttons,
                    )
                  ],
                ),
              )
            ),
            Positioned(
              left: 20,
              right: 20,
              child: CircleAvatar(
                backgroundColor: DialogBoxColors.avatarColor,
                radius: 40,
                child: icon,
              ),
            ),
          ],
        ),
    );
  }
}

