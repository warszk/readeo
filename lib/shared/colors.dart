import 'package:flutter/material.dart';

class AppColors {
  static final Color primaryColor = Color.fromRGBO(58, 66, 86, 1.0);
  static final Color secondaryColor = Color.fromRGBO(64, 75, 96, .9);
  static final Color primaryTextColor = Colors.white;
  static final Color secondaryTextColor = Colors.white24;
}

class DialogBoxColors {
  static final Color shadowColor = Colors.black26;
  static final avatarColor = Colors.blueAccent;
}

class TokenColors {
  static final Color level0 = Color.fromRGBO(173, 233, 255, 1.0);
  static final Color level1 = Color.fromRGBO(245, 164, 189, 1);
  static final Color level2 = Color.fromRGBO(245, 204, 169, 1);
  static final Color level3 = Color.fromRGBO(245, 225, 169, 1);
  static final Color level4 = Color.fromRGBO(230, 233, 169, 1);
  static final Color level5 = Color.fromRGBO(221, 255, 169, 1);
  static final Color level98 = Color.fromRGBO(255, 255, 255, 1);
  // static final Color level99 = Color.fromRGBO(240, 240, 240, 1);
  static final Color level99 = ReaderColors.bg;
  static final Color whitey = Color.fromRGBO(221, 221, 221, 1);
}

class ReaderColors {
  static final Color bg = Color(0xFFFBF0D9);
  static final Color textColor = Color(0xFF5F4B32);
}

