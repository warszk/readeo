import 'package:flutter/material.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/reader/providers/reader_token_store_provider.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/router/undefined.view.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/router/router.dart' as router;
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<MyDatabase>.value(value: MyDatabase(),),
        Provider<ReaderTokenStoreProvider>.value(value: ReaderTokenStoreProvider()) // simple store to get tokens, lame solution
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: HomeRoute,
        title: 'Intensivo',
        theme: ThemeData(
          primaryColor: AppColors.primaryColor
        ),
        onGenerateRoute: router.generateRoute,
        onUnknownRoute: (settings) => MaterialPageRoute(
          builder: (context) => UndefinedView(
            name: settings.name,
          )),
      )
    );
  }
}
