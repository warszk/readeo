import 'dart:async';

import 'package:readeo/db/database.dart';
import 'package:readeo/models/language.model.dart';

class LanguageProvider {

  final _stream = StreamController<List<LanguageModel>>.broadcast();
  Stream get languages => _stream.stream;
  MyDatabase db;
  StreamSubscription<List<DbLanguage>> _dbStream;

  setDb(MyDatabase db) {
    this.db = db;
  }

  listen() {
    _dbStream = db.languagesDao.watchLanguagesStream().listen((data) {
      _stream.sink.add(data.map((d) => LanguageModel.fromDB(d)).toList());
    });
  }


  dispose() {
    _stream.close();
    _dbStream.cancel();
  }
}
