
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/models/language.model.dart';
import 'package:readeo/languages/language_provider.dart';
import 'package:readeo/shared/colors.dart';

class LanguageListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<LanguageProvider>(
      create: (BuildContext context) => LanguageProvider(),
      child: _ScaffoldView(),
      dispose: (BuildContext context, LanguageProvider provider) => provider.dispose(),
    );
  }
}
class _ScaffoldView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final languageProvider = Provider.of<LanguageProvider>(context);
    final databaseProvider = Provider.of<MyDatabase>(context);
    languageProvider.setDb(databaseProvider);
    languageProvider.listen();

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: AppColors.primaryColor,
        title: Text("Languages"),
      ),
      body: _LanguageListStreamWidget(),
      bottomNavigationBar: Container(
        height: 55.0,
        child: BottomAppBar(
          color: AppColors.primaryColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(icon: Icon(Icons.home, color: Colors.white), onPressed: () {
                Navigator.of(context).pop();
              },),
              IconButton(icon: Icon(Icons.add_box, color: Colors.white), onPressed: () {
                // showDialog(
                //   context: context,
                //   builder: (BuildContext context) => NewLanguageWidget()
                // );
              },)
            ],
          ),
        ),
      ) ,
    );
  }
}

class _LanguageListStreamWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final languageProvider = Provider.of<LanguageProvider>(context);
    return StreamBuilder<List<LanguageModel>>(
      stream: languageProvider.languages,
      builder: (context, AsyncSnapshot<List<LanguageModel>> snapshot) {
        if(snapshot.hasData) {
          return _LanguageListViewBuilder(snapshot.data);
        } else if(snapshot.hasError) {
          return Center(child: Text(snapshot.error),);
        } else {
          return Center(child: CircularProgressIndicator(),);
        }
      },
    );
  }
}

class _LanguageListViewBuilder extends StatelessWidget {
  final List<LanguageModel> languages;
  _LanguageListViewBuilder(this.languages);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: languages.length,
      itemBuilder: (context, index) {
        return Card(
          elevation: 8.0,
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: AppColors.secondaryColor),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              leading: Container(
                padding: EdgeInsets.only(right: 12),
                decoration: BoxDecoration(
                  border: Border(right: BorderSide(width: 1.0, color: AppColors.secondaryTextColor))
                ),
                child: Icon(Icons.language),
              ),
              title: Text(languages[index].name, style: TextStyle(color: AppColors.primaryTextColor, fontWeight: FontWeight.bold)),
              subtitle: Text("${languages[index].code3} / ${languages[index].code2}", overflow: TextOverflow.ellipsis, style: TextStyle(color: AppColors.secondaryTextColor)),
              onTap: () {
                // Flushbar(
                //   message: "Editing is not possible yet.",
                //   duration: Duration(seconds: 2),
                // ).show(context);
              },
            ),
          ),
        );
      },
    );
  }
}

