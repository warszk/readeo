import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/router/reader_nav_arguments.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/shared/custom_dialog.widget.dart';
import 'package:readeo/models/book.model.dart';
import 'package:readeo/router/bookdescription_nav_arguments.dart';
import 'package:readeo/shelf/library_provider.dart';

class LocalShelfWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LibraryProvider libraryProvider = Provider.of<LibraryProvider>(context);
    return StreamBuilder<List<BookModel>>(
      initialData: libraryProvider.currentBookModels,
      stream: libraryProvider.bookListStream,
      builder: (context, AsyncSnapshot<List<BookModel>> snapshot) {
        if(snapshot.hasData) {
          return _BooksShelfGrid(snapshot.data);
        } else if(snapshot.hasError) {
          return Center(child: Text(snapshot.error),);
        } else {
          return Center(child: Text("Nothing here yet...", style: TextStyle(color: AppColors.primaryTextColor, fontSize: 21),),);
        }
      },
    );
  }
}



class _BooksShelfGrid extends StatelessWidget {
  final List<BookModel> books;

  _BooksShelfGrid(this.books);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      primary: false,
      slivers: <Widget>[
        SliverPadding(
          padding: EdgeInsets.all(16),
          sliver: SliverGrid.count(
            childAspectRatio: 2/3,
            crossAxisCount: 3,
            mainAxisSpacing: 20.0,
            crossAxisSpacing: 20.0,
            children: books.map((book) => _BookModelTile(book)).toList(),
          ),
        )
      ],
    );
  }
}

class _BookModelTile extends StatelessWidget {
  final BookModel bookEntry;
  _BookModelTile(this.bookEntry);
  @override
  Widget build(BuildContext context) {
    final LibraryProvider libraryProvider = Provider.of<LibraryProvider>(context);

    return Material(
      elevation: 15,
      shadowColor: Colors.yellow.shade900,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(BookDescriptionRoute, arguments: BookDescriptionNavArguments(bookEntry));
        },
        onDoubleTap: () {
          Navigator.of(context).pushNamed(ReaderRoute, arguments: ReaderNavArguments(bookEntry));
        },
        onLongPress: () {
          showDialog(
            context: context,
            builder: (context) {
              return CustomDialogWidget(
                icon: Icon(Icons.delete_forever, size: 40,),
                title: "Archive a book",
                body: Center(
                  child: RichText(
                    text: TextSpan(
                      text: "Are you sure you want to archive:\n",
                      style: TextStyle(color: AppColors.primaryColor, height: 1.5),
                      children:[
                        TextSpan(text: bookEntry.title, style: TextStyle(fontWeight: FontWeight.bold))
                      ]
                    ),
                    textAlign: TextAlign.center,
                  )
                ),
                buttons: ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      child: Text("Cancel"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    RaisedButton(
                      color: Colors.red,
                      child: Text("Archive"),
                      onPressed: () {
                        libraryProvider.archiveBook(bookEntry);
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ),
              );
            }
          );
        },
        child: Hero(
          tag: "${bookEntry.id}-${bookEntry.uuid}",
          child: Image.file(
            File(bookEntry.imagePath),
            fit: BoxFit.cover,
          )
        ),
      ),
    );
  }
}

