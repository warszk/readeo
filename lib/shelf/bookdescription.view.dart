import 'dart:io';

import 'package:flutter/material.dart';
import 'package:readeo/router/reader_nav_arguments.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/models/book.model.dart';
import 'package:readeo/router/bookdescription_nav_arguments.dart';

class BookDescriptionView extends StatelessWidget {
  final BookModel book;

  BookDescriptionView(BookDescriptionNavArguments args) :
    book = args.book;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Description"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _Header(book),
          Expanded(child: _DescriptionBox(book))
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  final BookModel book;

  _Header(this.book);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      color: AppColors.primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Hero(
            tag: "${book.id}-${book.uuid}",
            child: Image.file(
              File(book.imagePath),
              fit: BoxFit.cover,
              width: 150,
            )
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 20),
                Text(book.title, style: TextStyle(color: AppColors.primaryTextColor, fontSize: 21, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                Text(book.langCode, style: TextStyle(color: AppColors.secondaryTextColor)),
                SizedBox(height: 40),
                Align(
                  alignment: Alignment.bottomRight,
                  child: ButtonBar(
                    children: <Widget>[
                      RaisedButton(
                        child: Text("Read"),
                        color: Colors.green,
                        onPressed: () {
                          Navigator.of(context).pushNamed(ReaderRoute, arguments: ReaderNavArguments(book));
                        },
                      ),
                      RaisedButton(
                        child: Text("Archive"),
                        color: Colors.red,
                        onPressed: () {
                          // Flushbar(
                          //   message: "Not implemented yet",
                          //   duration: Duration(seconds: 2),
                          //   icon: Icon(Icons.error_outline),
                          // ).show(context);
                        },
                      )
                    ],
                  )
                )
              ],
            )
          )
        ],
      ),
    );
  }
}

class _DescriptionBox extends StatelessWidget {
  final BookModel book;
  _DescriptionBox(this.book);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      color: AppColors.secondaryColor,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Text(book.description ?? "No description", style: TextStyle(color: AppColors.primaryTextColor, fontSize: 18, height: 1.5), textAlign: TextAlign.justify,)
        )
      )
    );
  }
}
