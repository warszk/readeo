import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/router/routes.dart';
import 'package:readeo/shared/colors.dart';
import 'package:readeo/shelf/library_provider.dart';
import 'package:readeo/shelf/localshelf.widget.dart';

class ShelfView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<LibraryProvider>(
      create: (BuildContext context) => LibraryProvider(),
      child: _ScaffoldView(),
      dispose: (BuildContext context, LibraryProvider provider) => provider.dispose(),
    );
  }
}

class _ScaffoldView extends StatelessWidget {
    @override
  Widget build(BuildContext context) {
    final libraryProvider = Provider.of<LibraryProvider>(context);
    final dbProvider = Provider.of<MyDatabase>(context);
    libraryProvider.setDb(dbProvider);
    libraryProvider.listen();

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: AppColors.primaryColor,
        appBar: AppBar(
          elevation: 0.1,
          backgroundColor: AppColors.primaryColor,
          title: Text("Library"),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.settings), onPressed: () {})
          ],
        ),
        body: LocalShelfWidget(),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                child: Stack(children: <Widget>[
                  Positioned(
                      bottom: 12.0,
                      left: 16.0,
                      child: Text("Intensivo",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w500))),
                ]),
                decoration: BoxDecoration(
                  color: Colors.blue
                ),
              ),
              ListTile(
                title: Row(
                  children: [
                    Icon(Icons.list),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Tokens")
                    )
                  ]
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, TokensRoute);
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    Icon(Icons.language),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text("Languages")
                    )
                  ]
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, LanguagesRoute);
                },
              ),
            ],
          )
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, OpdsRoute);
          },
          child: Icon(Icons.add),
          mini: true,
        ),
      )
    );
  }
}

