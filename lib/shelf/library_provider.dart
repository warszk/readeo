import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

import 'package:readeo/db/database.dart';
import 'package:readeo/models/book.model.dart';


class LibraryProvider {
  MyDatabase db;

  List<BookModel> currentBookModels = List();

  //db stream
  final _dbStream = StreamController<List<BookModel>>.broadcast();
  Stream get bookListStream => _dbStream.stream;
  StreamSubscription watchBookStream;

  void setDb(MyDatabase db) async {
    this.db = db;
  }

  void listen() async {
    var path = (await getApplicationDocumentsDirectory()).path;

    watchBookStream = db.booksDao.watchBookStream().listen((data) {
      currentBookModels = data.map((d) => BookModel.fromBook(d, path: path)).toList();
      _dbStream.sink.add(currentBookModels);
    });
  }

  archiveBook(BookModel book) async {
    await db.booksDao.removeBook(book);
    final dir = Directory(book.rootFolder + "/${book.uuid}");
    await dir.delete(recursive: true);
    await Directory(book.rootFolder + "/${book.uuid}.epub").delete(recursive: true);
  }


  dispose() {
    _dbStream.close();
    watchBookStream.cancel();
  }
}
