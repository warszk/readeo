// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'toc.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Chapter _$ChapterFromJson(Map<String, dynamic> json) {
  return Chapter(
    name: json['name'] as String,
    href: json['href'] as String,
  );
}

Map<String, dynamic> _$ChapterToJson(Chapter instance) => <String, dynamic>{
      'name': instance.name,
      'href': instance.href,
    };
