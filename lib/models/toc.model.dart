import 'package:json_annotation/json_annotation.dart';

part 'toc.model.g.dart';

@JsonSerializable()
class Chapter {
  String name;
  String href;

  Chapter({this.name, this.href});

  factory Chapter.fromJson(Map<String, dynamic> json) =>
      _$ChapterFromJson(json);
  Map<String, dynamic> toJson() => _$ChapterToJson(this);
}

