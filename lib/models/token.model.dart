import 'package:flutter/cupertino.dart';
import 'package:readeo/db/database.dart';
import 'package:readeo/shared/colors.dart';

class TokenModel {
  int id;
  String name;
  String definition;
  String langcode;
  int level = 1;
  DateTime updatedAt;

  TokenModel();

  String get levelName {
    switch (level) {
      case 0:
        return "Unknown";
      case 1:
        return "Seen";
      case 2:
        return "Ring a bell";
      case 3:
        return "So so";
      case 4:
        return "Good";
      case 5:
        return "Known";
      case 98:
        return "Skip";
      case 99:
        return "Well known";
    }

    return "Unknown";
  }

  Color get color {
    switch (level) {
      case 0:
        return TokenColors.level0;
      case 1:
        return TokenColors.level1;
      case 2:
        return TokenColors.level2;
      case 3:
        return TokenColors.level3;
      case 4:
        return TokenColors.level4;
      case 5:
        return TokenColors.level5;
      case 98:
        return TokenColors.level99;
      case 99:
        return TokenColors.level99;
    }

    return TokenColors.level0;
  }

  DbToken toDbToken() {
    return DbToken(
        id: id,
        name: name.trim(),
        languageCode: langcode.trim(),
        definition: definition.trim(),
        level: level,
        updatedAt: updatedAt.millisecondsSinceEpoch);
  }

  TokenModel.fromToken(DbToken entity)
      : id = entity.id,
        name = entity.name,
        langcode = entity.languageCode,
        definition = entity.definition,
        level = entity.level,
        updatedAt = DateTime.fromMillisecondsSinceEpoch(entity.updatedAt);

  @override
  String toString() {
    return "<name: $name; definition: $definition; level: $level; language: $langcode>";
  }
}
