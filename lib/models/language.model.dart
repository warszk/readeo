import 'package:readeo/db/database.dart';

class LanguageModel {
  int id;
  String name;
  String code2;
  String code3;

  LanguageModel({this.name, this.code2, this.code3});

  LanguageModel.fromDB(DbLanguage lang) :
    id = lang.id,
    name = lang.name,
    code2 = lang.code2,
    code3 = lang.code3;

  DbLanguage toDbLanguage() {
    return DbLanguage(
      id: id,
      code3: code3,
      code2: code2,
      name: name
    );
  }
}
