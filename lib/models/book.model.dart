import 'package:readeo/db/database.dart';

class BookModel {
  int id;
  String uuid;
  String title;
  String description;
  String langCode;
  String rootFolder;
  String status;
  String type;
  int currentChapter;
  int currentLocation;
  int totalLength;

  String get imagePath => "$rootFolder/$uuid/cover";
  String get contentPath => "$rootFolder/$uuid.epub";
  String get folderPath => "$rootFolder/$uuid";

  BookModel(
      {this.uuid,
      this.title,
      this.description,
      this.langCode,
      this.status,
      this.type,
      this.currentChapter,
      this.currentLocation,
      this.totalLength,
      rootFolder});

  BookModel.fromBook(DbBook book, {String path})
      : id = book.id,
        uuid = book.uuid,
        title = book.title,
        description = book.description,
        langCode = book.languageCode,
        status = book.status,
        type = book.type,
        currentChapter = book.lastSeenChapter ?? 0,
        currentLocation = book.lastSeenPositionInChapter ?? 0,
        totalLength = book.totalByteLength,
        rootFolder = path;

  DbBook toDbBook() {
    return DbBook(
      id: this.id,
      title: this.title,
      description: this.description,
      uuid: this.uuid,
      languageCode: this.langCode,
      status: this.status,
      type: this.type,
      lastSeenChapter: this.currentChapter,
      lastSeenPositionInChapter: this.currentLocation
    );
  }
}
