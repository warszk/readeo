# readeo

Reader for language learning

### Compile

flutter run --release

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Use specification
move to use calibre as a local server
https://specs.opds.io/opds-1.2

http://feedbooks.com/
    - en / es / fr/ de / it


## TODO
logowanie / tworzenie konta
    - wykryc serwis
    - zalogowac sie / zarejestrowac sie

reader
    zapisywanie nieznanych slow na piewszej stronie nie dziala (bierze z drugiej strony)
    nie zapisuje definicji przy nowych slowach

    popracowac nad wyswietlaniem stron
         za duze marginesy na dole strony
         nazwa rozdzialu

    inny sposob wyswietlania rozdzialow
        wczytac wszystkie rozdzialy do rozdzilu w ktorym jestesmy?

    przekrecanie ekranu powinno przeliczac strony
    dodawanie przykladow zdan
        - na przycisk
        - po przejsciu strony wybieramy slowa na czerwono (0, 1) i zapisujemy do nich zdania
    lepsza obsluga odgrywania dzwieku
        - uzytkownik powinien miec mozliwosc wyboru silnika ewentualnie odgrywania domyslnie tego ktory ma ustawiony
    tlumaczenia
        - podlaczyc google translate api
        - podlaczyc deepl
        - podlaczyc wiktionary
        - podlaczyc wikipedie?
        - parsowac jakies slowniki online?
    dodawanie i wyswietlanie fraz zlozonych z kilku slow
dodac ustawienia
    - jezyk
    - fallback lang
    - logowanie
        firebase
tokeny
    - eksport / import tokenow do factory, trzeba bedzie to jakos inaczej wymyslec (isolates? service?)
    - sync
        - zapisywac w firebase? firestore?
        - albo lokalnie apka + baza
            mdns do discovery (bonjour)
    - wyswietlac pojedyncze slowa
        tlumaczenia
        zapamietane zdania

opsd - deprecated
    wyczyscic kod
    progres sciagania ksiazki
    zapamietywanie historii nawigacji przy przechodzeniu po kolejnych podstronach
    niektore epuby nie dzialaja, szukac innego rozwiazania, moze wrzucac do androida i tam przerabiac?


wszystkie modele przeniesc do database i od razu po zapytaniu przerabiac result na odpowiedni model?

1. Tokens / WordList
    - wybrac storage online (firebase?), moze cos lokalnie? serwer?
    - sort nie dziala

2. Library view
    - wczytywanie wiecej po dojsciu do konca
    - zapamietac ustawienia calibre i wczytac
    - archiwizowanie ksiazek

3. Reader
    - sforkowac epubreader i poprawic zeby czytal moje ksiazki
    - test on texts
        - english
        - polish
        - spanish
        - russian
        - japanese
        - chinese
        - arabic

4. Languages
    - nowy jezyk - widzet
    - usuwanie jezyka

5. OPDS
    - dodac nawigacje
    - parsowac linki
        - dodac do nawigacji
    - parsowac info o oplacie
    - w ogole zrobic parser opds
    - logowanie
    - zmerdzowac calibre z opds
    - sprawdzic kilka opsd
    - zintegrowac jakies fajne :)
