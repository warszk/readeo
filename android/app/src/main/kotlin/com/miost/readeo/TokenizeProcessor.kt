package com.miost.readeo

import android.icu.util.ULocale
import android.util.Log
import kotlinx.serialization.Serializable
import java.text.BreakIterator
import java.util.*
import kotlin.collections.ArrayList

@Serializable
enum class TokenType {
    WORD,
    OTHER
}

@Serializable
data class Token(val start: Int, val end: Int, val type: TokenType, val text: String)
@Serializable
data class Sentence(val start: Int, val end: Int, val tokens: List<Token>)


@Serializable
data class TokenizerResponse(val sentences: List<Sentence>, val position: Int)

class TokenizeProcessor {
    private var locale: Locale? = null

    private var currentTokenPosition: Int = 0
    private var onTokenizeProcessorListener: ((TokenizerResponse) -> Unit)? = null

    fun process(text: String, languageCode: String, position: Int) {
        currentTokenPosition = 0
        locale = ULocale.Builder().setLanguage(languageCode).build().toLocale()
        onTokenizeProcessorListener?.invoke(TokenizerResponse(splitToSentences(text), position))
    }

    private fun splitToSentences(text: String): ArrayList<Sentence> {
        val sentences: ArrayList<Sentence> = ArrayList()

        val iterator = BreakIterator.getSentenceInstance(locale)
        iterator.setText(text)
        var start = iterator.first()
        var end = iterator.next()

        while(end != BreakIterator.DONE) {
            val sentence = text.substring(start, end)
            val s = currentTokenPosition
            val tokens = splitTokens(sentence)
            val e = if(tokens.isEmpty()) { currentTokenPosition } else { tokens.last().end }
            sentences.add(Sentence(s, e, tokens))

            start = end
            end = iterator.next()
        }
        return sentences
    }

    private fun splitTokens(sentence: String): ArrayList<Token> {
        val words: ArrayList<Token> = ArrayList()
        val iterator = BreakIterator.getWordInstance(locale)
        iterator.setText(sentence)
        var start = iterator.first()
        var end = iterator.next()
        while(end != BreakIterator.DONE) {
            val word = sentence.substring(start, end)

            val e = currentTokenPosition + word.length
            if(isWord(word) && !isNumeric(word)) {
                words.add(Token(currentTokenPosition, e, TokenType.WORD, word))
            } else {
                words.add(Token(currentTokenPosition, e, TokenType.OTHER, word))
            }
            currentTokenPosition = e
            start = end
            end = iterator.next()
        }
        return words
    }

    private fun isWord(word: String): Boolean {
        return if (word.length == 1) {
            Character.isLetter(word[0])
        } else "" != word.trim { it <= ' ' }
    }

    private fun isNumeric(s: String?): Boolean {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+".toRegex())
    }

    fun setOnTokenizeProcessorListener(listener: (paragraphs: TokenizerResponse) -> Unit): TokenizeProcessor {
        onTokenizeProcessorListener = listener
        return this
    }


}
