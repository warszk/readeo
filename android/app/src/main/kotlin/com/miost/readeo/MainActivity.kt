package com.miost.readeo

import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.BasicMessageChannel
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.StringCodec
import io.flutter.plugins.GeneratedPluginRegistrant
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.json.JSONObject

const val ICU_TEXT_EVENT_CHANNEL = "com.miost.readeo/icu/events"
const val ICU_TEXT_DATA_CHANNEL = "com.miost.readeo/icu/data"

class MainActivity: FlutterActivity() {
    private lateinit var icuTextEventChannel: EventChannel
    private lateinit var icuTextDataChannel: BasicMessageChannel<String>
    private val tokenizer: TokenizeProcessor = TokenizeProcessor()


    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)

        registerFlutterCallbacks(flutterEngine)
//        registerEventChannel(flutterEngine)
    }

    private fun registerEventChannel(flutterEngine: FlutterEngine) {
        icuTextEventChannel = EventChannel(flutterEngine.dartExecutor, ICU_TEXT_EVENT_CHANNEL)
        icuTextEventChannel.setStreamHandler(object : EventChannel.StreamHandler {
            override fun onListen(p0: Any?, sink: EventChannel.EventSink?) {
//                tokenizer.setOnTokenizeProcessorListener {
//                    sink?.success(Json.stringify(TokenizerResponse.serializer(), it))
//                }
            }

            override fun onCancel(p0: Any?) {
//                dispose()
            }
        })

    }

    private fun registerFlutterCallbacks(flutterEngine: FlutterEngine) {
        icuTextDataChannel = BasicMessageChannel(flutterEngine.dartExecutor, ICU_TEXT_DATA_CHANNEL, StringCodec.INSTANCE)

        icuTextDataChannel.setMessageHandler { message, reply ->
            message?.let {
                val received = JSONObject(message)

                val msgType = received.getString("type")

                when (msgType) {
                    "tokenize" -> {
                        val text = received.getString("text")
                        val languageCode = received.getString("languageCode")
                        val pos = received.getInt("position")

                        tokenizer.setOnTokenizeProcessorListener {
                            val l = Json.stringify(TokenizerResponse.serializer(), it)
                            icuTextDataChannel.send(l)
                        }

                        tokenizer.process(text, languageCode, pos)
                    }
                    else -> {
                        // do nothing for now
                    }
                }
            }
        }
    }
}
